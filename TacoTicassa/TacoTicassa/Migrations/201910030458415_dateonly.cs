namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateonly : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime(storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime());
        }
    }
}
