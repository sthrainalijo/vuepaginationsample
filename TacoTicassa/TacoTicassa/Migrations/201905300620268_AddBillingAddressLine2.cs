namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBillingAddressLine2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "BillingAddressLine2", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "BillingAddressLine2");
        }
    }
}
