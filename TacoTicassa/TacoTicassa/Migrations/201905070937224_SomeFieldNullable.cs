namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeFieldNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "CreatedOn", c => c.DateTime());
            AlterColumn("dbo.AspNetUsers", "CreatedByBranchID", c => c.Int());
            AlterColumn("dbo.AspNetUsers", "IsDeliveryAddressSame", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "IsDeliveryAddressSame", c => c.Boolean(nullable: false));
            AlterColumn("dbo.AspNetUsers", "CreatedByBranchID", c => c.Int(nullable: false));
            AlterColumn("dbo.AspNetUsers", "CreatedOn", c => c.DateTime(nullable: false));
        }
    }
}
