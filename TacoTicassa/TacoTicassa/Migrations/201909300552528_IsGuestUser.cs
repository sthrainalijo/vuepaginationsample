namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsGuestUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsFirstTimeRegisteration", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "IsGuestUser", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsGuestUser");
            DropColumn("dbo.AspNetUsers", "IsFirstTimeRegisteration");
        }
    }
}
