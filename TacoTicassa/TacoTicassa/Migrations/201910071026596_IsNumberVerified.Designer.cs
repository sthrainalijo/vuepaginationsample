// <auto-generated />
namespace TacoTicassa.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class IsNumberVerified : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(IsNumberVerified));
        
        string IMigrationMetadata.Id
        {
            get { return "201910071026596_IsNumberVerified"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
