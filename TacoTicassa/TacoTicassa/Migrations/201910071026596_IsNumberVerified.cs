namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsNumberVerified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsNumberVerified", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsNumberVerified");
        }
    }
}
