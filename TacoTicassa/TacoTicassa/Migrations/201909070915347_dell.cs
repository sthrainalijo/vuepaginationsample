namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dell : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "DateOfBirth");
            DropColumn("dbo.AspNetUsers", "Nationality");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Nationality", c => c.String());
            AddColumn("dbo.AspNetUsers", "DateOfBirth", c => c.String());
        }
    }
}
