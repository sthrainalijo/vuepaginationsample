namespace TacoTicassa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dobdelete : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "DateOfBirth");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
