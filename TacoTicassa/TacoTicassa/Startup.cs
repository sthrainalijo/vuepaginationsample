﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TacoTicassa.Startup))]
namespace TacoTicassa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
