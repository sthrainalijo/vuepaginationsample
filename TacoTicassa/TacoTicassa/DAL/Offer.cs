﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TacoTicassa.Models;

namespace TacoTicassa.DAL
{
    public class Offer
    {
        TacoTicassaDbEntities Db = new TacoTicassaDbEntities();
        public List<ProductOffersWithDetails> GetOfferInfo(int? BranchID,string LoginUserID, string term/*, int? start, int? end*/)
        {
        

            var BranchIDString = BranchID.ToString();
            BranchIDString = BranchIDString == "0" ? "" : BranchIDString;

            List<ProductOffersWithDetails> FilteredProductInfo = new List<ProductOffersWithDetails>();
            string ConnectionString = Common.GetConnectionString();

            bool BranchFilter;
            if (BranchIDString != "")
            {
                BranchFilter = true;
            }
            else
            {
                BranchFilter = false;
            }


            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("ProductOfferInfo", param: new { BranchID = BranchIDString, searchKey = term /*, BranchFilter= BranchFilter, start = start, end = end */}, commandType: CommandType.StoredProcedure);

                var ProductOfferDetails = reader.Read<ProductOffersWithDetails>().ToList();

               
                foreach(var Item in ProductOfferDetails)
                {
                    
                    var UserOrders = Db.OrderTbls.Where(x => x.CreatedByUserID == LoginUserID).FirstOrDefault();
                    int? CouponcodeUsage = Db.OrderTbls.Where(x => x.CreatedByUserID == LoginUserID && x.OfferIDf == Item.ProductOfferID).Count();

                    Item.AlreadyUsedOfferCount = CouponcodeUsage;
                    if(Item.OfferUsageCount==null)
                    {
                        Item.OfferUsageCount = 0;
                    }
                    if(UserOrders==null && Item.OfferType == "Offer By User"&&Item.IsthisOfferForUserFirstOrder == true)
                    {
                        Item.OfferUsageCount = 1;
                    }
                    //if(CouponcodeUsage==Item.OfferUsageCount)
                    //{
                    //    continue;
                    //}
                    //else
                    //{
                        string CurrentTimeString = Common.GetCurrentDateTime().ToString("HH:mm");
                        TimeSpan CurrentTimeInHHMM = TimeSpan.Parse(CurrentTimeString);
                        string CurrentWeekDay = Common.GetCurrentDateTime().DayOfWeek.ToString().ToLower(); ;
                        DateTime? CurrentDateOnly = Common.GetCurrentDateTime().Date;
                       

                        DateTime? OfferDateOnly = Item.OfferDate.HasValue ? Item.OfferDate.Value.Date : DateTime.MinValue.Date;
                        DateTime? OfferCreatedOnDateOnly = Item.CreatedOn.Date;
                        DateTime? OfferStartDateOnly = Item.OfferStartDate.HasValue ? Item.OfferStartDate.Value.Date : DateTime.MinValue.Date;
                        DateTime? OfferEndDateOnly = Item.OfferEndDate.HasValue ? Item.OfferEndDate.Value.Date : DateTime.MinValue.Date;
                        if (Item.OfferType == "Offer By User" && Item.IsthisOfferForUserFirstOrder == true && UserOrders != null)
                        {
                            continue;
                        }   

                        else if (Item.OfferType == "Offer By User" && Item.IsthisOfferForUserFirstOrder != true && Item.OfferForUserID != LoginUserID)
                        {
                            continue;
                        }
                        else if (Item.OfferType == "Offer By Hour" && (CurrentDateOnly != OfferCreatedOnDateOnly || Item.OfferStartHour > CurrentTimeInHHMM || Item.OfferEndHour < CurrentTimeInHHMM))
                        {
                            continue;

                        }
                        else if (Item.OfferType == "Offer By Date" && CurrentDateOnly != OfferDateOnly)
                        {
                            continue;

                        }
                        else if (Item.OfferType == "Offer By Season" && (OfferStartDateOnly > CurrentDateOnly || OfferEndDateOnly < CurrentDateOnly))
                        {
                            continue;

                        }
                        else if (Item.OfferType == "Offer By Month" && Item.OfferMonthdate != Common.GetCurrentDateTime().Day.ToString())
                        {
                            continue;

                        }
                        else if (Item.OfferType == "Offer By Week" && Item.OfferWeek != CurrentWeekDay)
                        {
                            continue;

                        }
                        else
                        {
                            FilteredProductInfo.Add(Item);
                        }
                        Item.OfferWeek = Item.OfferWeek == null ? "" : Item.OfferWeek.ToUpper(); ;
                    //}
                   
                }
                
                return FilteredProductInfo;
            }

        }

        private ProductOffersWithDetails Json(List<ProductOffersWithDetails> productOfferDetails, object allowGet)
        {
            throw new NotImplementedException();
        }
    }

    public class ProductOffersWithDetails : ProductOfferTbl
    {
        public string ProductName { get; set; }
        public string OfferForuserName { get; set; }
        public string OfferDatePart { get; set; }
        public string OfferStartDatePart { get; set; }
        public string OfferEndDatePart { get; set; }
        public string OfferStartHourPart { get; set; }
        public string OfferEndHourPart { get; set; }
        public int? AlreadyUsedOfferCount { get; set; }

    }
}