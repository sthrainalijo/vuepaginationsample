﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TacoTicassa.Models;


namespace TacoTicassa.DAL
{
    public class Product
    {
        public ProductDetailsInfo GetProductData(int? BranchID,string searchKey,int?CategoryID,int? start,int? end)
        {
            var BranchIDString = BranchID.ToString();
            BranchIDString = BranchIDString == "0" ? "" : BranchIDString;
            var CategoryIDString = CategoryID.ToString();
            CategoryIDString = CategoryIDString == "0" ? "" : CategoryIDString;

            string ConnectionString = Common.GetConnectionString();
            ProductDetailsInfo ProductDetailsInfoData = new ProductDetailsInfo();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("AllProductInfo", param: new { BranchID = BranchIDString, searchKey= searchKey, CategoryID= CategoryIDString,start=start,end=end }, commandType: CommandType.StoredProcedure);

                var ProductCategoryDetailsData = reader.Read<ProductCategoryTbl>().ToList();
                ProductDetailsInfoData.ProductDetailsData = reader.Read<ProductDetails>().ToList();
                return ProductDetailsInfoData;
            }
        }

        public ProductDetails GetSingleProductData(int ProductID,int? ProductVariantID)
        {
            string ConnectionString = Common.GetConnectionString();
            ProductDetailsInfo ProductDetailsInfoData = new ProductDetailsInfo();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("ProductInfo", param: new { ProductID= ProductID , ProductVariantID = ProductVariantID }, commandType: CommandType.StoredProcedure);


                var ProductDetailsData = reader.Read<ProductDetails>().FirstOrDefault();
                return ProductDetailsData;
            }
        }

        public List<ProductCategoryDetailsInfo> GetCategoryProductData(int? BranchID, string searchKey, int? CategoryID, int? start, int? end)
        {
            var BranchIDString = BranchID.ToString();
            BranchIDString = BranchIDString == "0" ? "" : BranchIDString;
            var CategoryIDString = CategoryID.ToString();
            CategoryIDString = CategoryIDString == "0" ? "" : CategoryIDString;

            string ConnectionString = Common.GetConnectionString();
            List<ProductCategoryDetailsInfo> CategoryDetailsInfoData = new List<ProductCategoryDetailsInfo>();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("AllProductInfo", param: new { BranchID = BranchIDString, searchKey = searchKey, CategoryID = CategoryIDString, start= start,end=end }, commandType: CommandType.StoredProcedure);

                var ProductCategoryDetailsData = reader.Read<ProductCategoryTbl>().ToList();
                var ProductDetailsData = reader.Read<CategoryProductDetails>().ToList();
                foreach(var Item in ProductCategoryDetailsData)
                {
                    var ProductCategoryList = ProductDetailsData.Where(x => x.ProductCategoryIDf == Item.ProductCategoryID).ToList();
                    ProductCategoryDetailsInfo NewCategory = new ProductCategoryDetailsInfo();
                    NewCategory.ProductCategoryID =Item.ProductCategoryID;
                    NewCategory.ProductCategoryName = Item.ProductCategoryName;
                    NewCategory.CategoryProductDetailsData = ProductCategoryList;
                    CategoryDetailsInfoData.Add(NewCategory);

                }

                return CategoryDetailsInfoData;
            }
        }
        public class ProductCategoryDetailsInfo:ProductCategoryTbl
        {
            public List<CategoryProductDetails> CategoryProductDetailsData = new List<CategoryProductDetails>();
        }

        public class ProductDetailsInfo
        {
            public List<ProductDetails> ProductDetailsData = new List<ProductDetails>();
            public CartTotal CartTotal = new CartTotal();
        }

        public class ProductDetails : ProductTbl
        {
            public int? TotalCount { get; set; }
            public string ProductCategoryName { get; set; }
            public string ProductCategoryImage { get; set; }
            public decimal? TaxPercentage { get; set; }
        


        }
        public class CategoryProductDetails : ProductTbl
        {
            public decimal? TaxPercentage { get; set; }


        }
        public class CartTotal
        {
            public string ProductCategoryName { get; set; }
            public string ProductCategoryImage { get; set; }
            public decimal? TotalAmount { get; set; }
            public decimal? TotalQty { get; set; }
            public decimal? TotalTaxAmount { get; set; }
            public decimal? NetAmount { get; set; }
        }
    }


}