﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TacoTicassa.Models;

namespace TacoTicassa.DAL
{
    public class Cart
    {
        TacoTicassaDbEntities Db = new TacoTicassaDbEntities();
        public CartDetailsInfo GetCartInfo(int CartID)
        {
            decimal? TotalAmount = 0, NetAmount = 0, OfferTotal = 0, TotalAdditionAmount = 0;
            string ConnectionString = Common.GetConnectionString();
            bool IsComboMeal = false;
            CartDetailsInfo CartDetailsInfoData = new CartDetailsInfo();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("CartInfo", param: new { CartID = CartID }, commandType: CommandType.StoredProcedure);


                CartDetailsInfoData.CartTblInfo = reader.Read<CartTbl>().FirstOrDefault();
                CartDetailsInfoData.RestaurantSettings = reader.Read<RestaurantSettingsTbl>().FirstOrDefault();
                CartDetailsInfoData.CartDetailsTblInfo = reader.Read<CartDetails>().ToList();
                CartDetailsInfoData.ProductOfferTblInfo = reader.Read<OfferDetails>().FirstOrDefault();
                CartDetailsInfoData.CartAdditionDetailsInfo = reader.Read<CartAdditionDetails>().ToList();
                CartDetailsInfoData.ProdcutAdditionItemInfo = reader.Read<ProdcutAdditionItemView>().ToList();
                CartDetailsInfoData.ProdcutFillingInfo = reader.Read<ProdcutFillingView>().ToList();
                CartDetailsInfoData.CartUpgradeMealsInfo = reader.Read<CartUpgradeMealsInfo>().ToList();

                foreach (var Item in CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.IsAddition == true).ToList())
                {

                    TotalAdditionAmount += Item.TotalAmount;
                }
              
                var ComboMealsList = Db.ComboDetailTbls.Where(x=>x.CartIDf==CartID).ToList();
                decimal AdditionalQty = 0;
                foreach (var Item in CartDetailsInfoData.CartDetailsTblInfo)
                {
                    var AdditionalQtyDetails = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf).Select(x => x.Qty).FirstOrDefault();
                    if (Item.ComboDetailsId == null || Item.ComboDetailsId == 0)
                    {
                        Item.IsComboMeal = false;
                        IsComboMeal = false;
                        TotalAmount += Item.SubTotalAmount;
                        NetAmount += Item.SubNetAmount;
                    }
                    else
                    {
                        decimal? SubAmount = ComboMealsList.Where(x => x.ComboDetailId == Item.ComboDetailsId).Select(x => x.ComboPrice).FirstOrDefault();
                        Item.Qty = ComboMealsList.Where(x => x.ComboDetailId == Item.ComboDetailsId).Select(x => x.ComboQty).FirstOrDefault();
                        SubAmount = SubAmount * Item.Qty;
                        Item.SubTotalAmount = SubAmount;
                        Item.SubNetAmount = SubAmount;
                        TotalAmount += SubAmount;
                        NetAmount += SubAmount;
                        IsComboMeal = true;
                        Item.IsComboMeal = true;
                    }
                    if (IsComboMeal == false)
                    {
                        if (CartDetailsInfoData.CartAdditionDetailsInfo != null)
                        {
                            var SingleAdditions = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsAddition == true).ToList();
                            Item.SingleProdcutAdditionInfo = SingleAdditions;
                            var SingleFillings = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsFilling == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            Item.SingleProdcutFillingInfo = SingleFillings;
                            var SingleSuaces = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsSauce == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            Item.SingleProdcutSauceInfo = SingleSuaces;

                            var SingleUpdateMealItems = CartDetailsInfoData.CartUpgradeMealsInfo.Where(x => x.CartDetailsIDf == Item.CartDetailsID && x.CartIDf == CartID).ToList();

                            string UpdateMealItemString = "";
                            foreach (var UpdateMeal in SingleUpdateMealItems)
                            {

                                string MealType = "";
                                if (UpdateMeal.IsDrink == true)
                                {
                                    MealType = "Drink";
                                }
                                if (UpdateMeal.IsSideDish == true)
                                {
                                    MealType = "SideDish";
                                }
                                if (UpdateMealItemString == "")
                                {
                                    UpdateMealItemString = UpdateMeal.UpdateMealItemName + "(" + MealType + ")";
                                }
                                else
                                {
                                    UpdateMealItemString += "," + UpdateMeal.UpdateMealItemName + "(" + MealType + ")";
                                }


                            }
                            Item.ProductUpdatemealDesc = UpdateMealItemString;
                            if (Item.ProductVariantID != 0)
                            {
                                var VariantInfo = Db.ProdcutVariantViews.Where(x => x.ProductVariantID == Item.ProductVariantID).FirstOrDefault();
                                if (VariantInfo != null)
                                {
                                    Item.ProductName = Item.ProductName + "-" + VariantInfo.VariantName;
                                }
                            }
                            if (Item.IsUpdatemealExists == true)
                            {
                                Item.ProductName = Item.ProductName + "(Upgraded To Meal)";
                            }

                            var Fillingtring = "";
                            var Saucetring = "";

                            if (SingleFillings != null && SingleFillings.Count != 0)
                            {

                                int Count = 1;
                                foreach (var filling in SingleFillings)
                                {
                                    Fillingtring += filling.ProductName;
                                    if (Count != SingleFillings.Count())
                                    {
                                        Fillingtring += ", ";
                                    }

                                    Count++;
                                }

                            }


                            if (SingleSuaces != null && SingleSuaces.Count != 0)
                            {

                                int Count = 1;
                                foreach (var Sauce in SingleSuaces)
                                {
                                    Saucetring += Sauce.ProductName;
                                    if (Count != SingleSuaces.Count())
                                    {
                                        Saucetring += ", ";
                                    }

                                    Count++;
                                }

                            }

                            Item.ProductFillingDesc = Fillingtring;
                            Item.ProductSauceDesc = Saucetring;
                        }

                    }
                    Item.ComboMealsProductInfo = new List<ComboMealsCartDetailsModel>();
                    if (IsComboMeal==true)
                    {
                        var ProductList = new List<ProductTbl>();
                        var AdditionalItems = new List<CartAdditionDetails>();
                        using (IDbConnection connection = new SqlConnection(ConnectionString))
                        {

                            var reader1 = connection.QueryMultiple("GetSubProduct", param: new { CartID = CartID, ProductId = Item.ProductIDf }, commandType: CommandType.StoredProcedure);
                            ProductList = reader1.Read<ProductTbl>().ToList();
                            AdditionalItems = reader1.Read<CartAdditionDetails>().ToList();
                        }
                        foreach (var Product in ProductList)
                        {
                            ComboMealsCartDetailsModel AdditionalItem = new ComboMealsCartDetailsModel();
                            AdditionalItem.SubProductName = Product.ProductName;
                             var SingleFillings = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsFilling == true&&x.CartDetailsIDf==Item.CartDetailsID).FirstOrDefault();
                            if(SingleFillings!=null)
                            AdditionalItem.ProductFillingDesc = SingleFillings.ProductName;
                            var SingleSauce = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsSauce == true && x.CartDetailsIDf == Item.CartDetailsID).FirstOrDefault();
                           if(SingleSauce!=null)
                            AdditionalItem.ProductSauceDesc = SingleSauce.ProductName;
                            //AdditionalItem.ProductAdditionDescription = new List<ProductAdditionalDescModel>();
                            //var SingleAddition = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsAddition == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            //foreach(var AdditionObj in SingleAddition)
                            //{
                            //    var AdditionCombostring = "";
                            //    var Addition = Db.ProductAdditionItemMapTbls.Where(x => x.ProductIDf == Product.ProductID && x.ProductAdditionItemIDf == AdditionObj.CartAdditionItemIDf).FirstOrDefault();
                            //    if (Addition != null)
                            //    {
                            //        if (Addition.IsThisFreeItem == true)
                            //        {
                            //            int Count = 1;

                            //            string FreeQty = Convert.ToInt32(AdditionObj.FreeQty).ToString();
                            //            AdditionCombostring += AdditionObj.ProductName + "(" + FreeQty + ")";
                            //            if (Count != SingleAddition.Count())
                            //            {
                            //                AdditionCombostring += ", ";
                            //            }

                            //            Count++;
                            //        }
                            //    }
                            //    ProductAdditionalDescModel NewObj = new ProductAdditionalDescModel();
                            //    NewObj.ProductAdditionDesc = AdditionCombostring;
                            //    AdditionalItem.ProductAdditionDescription.Add(NewObj);
                               
                            //}
                            Item.ComboMealsProductInfo.Add(AdditionalItem);
                        }


                    }
                    var Additionstring = "";
                    var FreeAdditions = Db.ProdcutAdditionItemViews.Where(x => x.ProductID == Item.ProductIDf).ToList();

                    if (FreeAdditions != null && FreeAdditions.Count != 0)
                    {
                        var FreeListOfAdditions = FreeAdditions.Where(x => x.IsThisFreeItem == true).ToList();

                        int Count = 1;
                        foreach (var addtion in FreeListOfAdditions)
                        {
                            string FreeQty = Convert.ToInt32(addtion.FreeQty).ToString();
                            Additionstring += addtion.ProductAdditionItemName + "(" + FreeQty + ")";
                            if (Count != FreeAdditions.Count())
                            {
                                Additionstring += ", ";
                            }

                            Count++;
                        }

                    }

                    //var FreeFillings = Db.ProdcutFillingViews.Where(x => x.ProductID == Item.ProductIDf).ToList();


                    //if (FreeFillings != null && FreeFillings.Count != 0)
                    //{
                    //    int Count = 1;
                    //    foreach (var filling in FreeFillings)
                    //    {
                    //        if (Additionstring != "")
                    //        {
                    //            Additionstring += ", ";
                    //        }

                    //        string FreeQty = Convert.ToInt32(filling.FreeQty).ToString();
                    //        Additionstring += filling.ProductFillingItemName + "(" + FreeQty + ")";

                    //        if (Count != FreeFillings.Count())
                    //        {
                    //            Additionstring += ", ";
                    //        }

                    //        Count++;
                    //    }

                    //}

                    Item.ProductAdditionDesc = Additionstring;
                    //TotalAmount += Item.SubTotalAmount;
                    ////TotalTaxAmount += Item.SubTotalTaxAmount;
                    //NetAmount += Item.SubNetAmount;

                   

                }
                //if (CartDetailsInfoData.CartTblInfo.TotalOfferAmount != null)
                //{
                //    OfferTotal = CartDetailsInfoData.CartTblInfo.TotalOfferAmount;
                //}


                decimal? NetWithAdditions = NetAmount + TotalAdditionAmount;
                if (CartDetailsInfoData.ProductOfferTblInfo != null)
                {
                    if (CartDetailsInfoData.ProductOfferTblInfo.OfferAmount != null || CartDetailsInfoData.ProductOfferTblInfo.OfferPercentage != null)
                    {
                        if (CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Product" || CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Month" || CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Week" || CartDetailsInfoData.ProductOfferTblInfo.IsOfferAmountTake == true)
                        {
                            OfferTotal = CartDetailsInfoData.ProductOfferTblInfo.OfferAmount;
                        }
                        else
                        {
                            OfferTotal = (NetWithAdditions * CartDetailsInfoData.ProductOfferTblInfo.OfferPercentage) / 100;
                        }
                    }
                }
                decimal TaxCalc = 0;
                decimal? CartOfferAmount = OfferTotal;
                decimal? NetAfterDiscount = NetWithAdditions - CartOfferAmount;

                decimal? RestaurantTaxRate = CartDetailsInfoData.RestaurantSettings.TaxPercentage;
                if(NetWithAdditions==null)
                {
                    NetWithAdditions = 0;
                }
                // TaxCalc = Decimal.Parse(((NetAfterDiscount * RestaurantTaxRate) / 100).ToString());
                TaxCalc = Math.Round((((Decimal.Parse(NetWithAdditions.ToString())) - ((Decimal.Parse(NetWithAdditions.ToString())) * (100 / (100 + ((Decimal.Parse(RestaurantTaxRate.ToString())))))))), 2);
                TaxCalc = Math.Round(TaxCalc, 2);
                

                //decimal? NetAfterTax = NetAfterDiscount + TaxCalc;

                CartDetailsInfoData.CartTblInfo.TotalAmount = TotalAmount;


                CartDetailsInfoData.CartTblInfo.TotalTaxAmount = TaxCalc;

                CartDetailsInfoData.CartTblInfo.TotalDiscountAmount = Math.Round(Decimal.Parse(OfferTotal.ToString()), 2);
                CartDetailsInfoData.CartTblInfo.TotalAdditionsAmount = TotalAdditionAmount;

                CartDetailsInfoData.CartTblInfo.DeliveryCharge = CartDetailsInfoData.CartTblInfo.DeliveryCharge ?? 0;

                decimal? NetAfterDiscountWithDeliveryCharge = NetAfterDiscount + CartDetailsInfoData.CartTblInfo.DeliveryCharge;
                if(NetAfterDiscountWithDeliveryCharge==null)
                {
                    NetAfterDiscountWithDeliveryCharge = 0;
                }
                CartDetailsInfoData.CartTblInfo.NetAmount = Math.Round(Decimal.Parse(NetAfterDiscountWithDeliveryCharge.ToString()), 2);

                //UpdateCart Tbl
                var UpdateCart = Db.CartTbls.Where(x => x.CartID == CartID).FirstOrDefault();
                UpdateCart.TotalAmount = TotalAmount??0;
                UpdateCart.TotalTaxAmount = TaxCalc;
                UpdateCart.TotalDiscountAmount = OfferTotal;
                UpdateCart.TotalAdditionsAmount = TotalAdditionAmount;
                UpdateCart.NetAmount = NetAfterDiscount;
                Db.SaveChanges();

                //CartDetailsInfoData.CartAdditionDetailsInfo = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.IsAddition == true).ToList();


                return CartDetailsInfoData;
            }
        }
        public CartDetailsInfo GetCartComboInfo(int CartID)
        {
            decimal? TotalAmount = 0, NetAmount = 0, OfferTotal = 0, TotalAdditionAmount = 0;
            string ConnectionString = Common.GetConnectionString();
            CartDetailsInfo CartDetailsInfoData = new CartDetailsInfo();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("ComboCartInfo", param: new { CartID = CartID }, commandType: CommandType.StoredProcedure);


                CartDetailsInfoData.CartTblInfo = reader.Read<CartTbl>().FirstOrDefault();
                 CartDetailsInfoData.RestaurantSettings = reader.Read<RestaurantSettingsTbl>().FirstOrDefault();
                CartDetailsInfoData.CartDetailsTblInfo = reader.Read<CartDetails>().ToList();
                CartDetailsInfoData.CartAdditionDetailsInfo = reader.Read<CartAdditionDetails>().ToList();
                CartDetailsInfoData.ComboDetailsInfo = reader.Read<ComboDetailTbl>().ToList();
                foreach (var Item in CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.IsAddition == true).ToList())
                {

                    TotalAdditionAmount += Item.TotalAmount;
                }
                foreach (var Item in CartDetailsInfoData.CartDetailsTblInfo)
                {
                    if (Item.ComboDetailsId == null || Item.ComboDetailsId == 0)
                    {
                        TotalAmount += Item.SubTotalAmount;
                        NetAmount += Item.SubNetAmount;
                        Item.IsComboMeal = false;
                    }
                    else
                    {
                        decimal? SubAmount = CartDetailsInfoData.ComboDetailsInfo.Where(x => x.ComboDetailId == Item.ComboDetailsId && x.CartIDf == Item.CartIDf).Select(x => x.ComboPrice).FirstOrDefault();
                        Item.Qty = CartDetailsInfoData.ComboDetailsInfo.Where(x => x.ComboDetailId == Item.ComboDetailsId && x.CartIDf == Item.CartIDf).Select(x => x.ComboQty).FirstOrDefault();
                        SubAmount = SubAmount * Item.Qty;
                        Item.SubTotalAmount = SubAmount;
                        Item.SubNetAmount = SubAmount;
                        TotalAmount += SubAmount;
                        NetAmount += SubAmount;
                        Item.IsComboMeal = true;
                    }
                    if (Item.IsComboMeal == false)
                    {
                        if (CartDetailsInfoData.CartAdditionDetailsInfo != null)
                        {
                            var SingleAdditions = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsAddition == true).ToList();
                            Item.SingleProdcutAdditionInfo = SingleAdditions;
                            var SingleFillings = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsFilling == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            Item.SingleProdcutFillingInfo = SingleFillings;
                            var SingleSuaces = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.ProductIDf == Item.ProductIDf && x.IsSauce == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            Item.SingleProdcutSauceInfo = SingleSuaces;

                            var SingleUpdateMealItems = CartDetailsInfoData.CartUpgradeMealsInfo.Where(x => x.CartDetailsIDf == Item.CartDetailsID && x.CartIDf == CartID).ToList();

                            string UpdateMealItemString = "";
                            foreach (var UpdateMeal in SingleUpdateMealItems)
                            {

                                string MealType = "";
                                if (UpdateMeal.IsDrink == true)
                                {
                                    MealType = "Drink";
                                }
                                if (UpdateMeal.IsSideDish == true)
                                {
                                    MealType = "SideDish";
                                }
                                if (UpdateMealItemString == "")
                                {
                                    UpdateMealItemString = UpdateMeal.UpdateMealItemName + "(" + MealType + ")";
                                }
                                else
                                {
                                    UpdateMealItemString += "," + UpdateMeal.UpdateMealItemName + "(" + MealType + ")";
                                }


                            }
                            Item.ProductUpdatemealDesc = UpdateMealItemString;
                            if (Item.ProductVariantID != 0)
                            {
                                var VariantInfo = Db.ProdcutVariantViews.Where(x => x.ProductVariantID == Item.ProductVariantID).FirstOrDefault();
                                if (VariantInfo != null)
                                {
                                    Item.ProductName = Item.ProductName + "-" + VariantInfo.VariantName;
                                }
                            }
                            if (Item.IsUpdatemealExists == true)
                            {
                                Item.ProductName = Item.ProductName + "(Upgraded To Meal)";
                            }

                            var Fillingtring = "";
                            var Saucetring = "";

                            if (SingleFillings != null && SingleFillings.Count != 0)
                            {

                                int Count = 1;
                                foreach (var filling in SingleFillings)
                                {
                                    Fillingtring += filling.ProductName;
                                    if (Count != SingleFillings.Count())
                                    {
                                        Fillingtring += ", ";
                                    }

                                    Count++;
                                }

                            }


                            if (SingleSuaces != null && SingleSuaces.Count != 0)
                            {

                                int Count = 1;
                                foreach (var Sauce in SingleSuaces)
                                {
                                    Saucetring += Sauce.ProductName;
                                    if (Count != SingleSuaces.Count())
                                    {
                                        Saucetring += ", ";
                                    }

                                    Count++;
                                }

                            }

                            Item.ProductFillingDesc = Fillingtring;
                            Item.ProductSauceDesc = Saucetring;
                        }
                    }
                    Item.ComboMealsProductInfo = new List<ComboMealsCartDetailsModel>();
                    if (Item.IsComboMeal == true)
                    {
                        var ProductList = new List<ProductTbl>();
                        var AdditionalItems = new List<CartAdditionDetails>();
                        using (IDbConnection connection = new SqlConnection(ConnectionString))
                        {

                            var reader1 = connection.QueryMultiple("GetSubProduct", param: new { CartID = CartID, ProductId = Item.ProductIDf }, commandType: CommandType.StoredProcedure);
                            ProductList = reader1.Read<ProductTbl>().ToList();
                            AdditionalItems = reader1.Read<CartAdditionDetails>().ToList();
                        }
                        foreach (var Product in ProductList)
                        {
                            ComboMealsCartDetailsModel AdditionalItem = new ComboMealsCartDetailsModel();
                            AdditionalItem.SubProductName = Product.ProductName;
                            var SingleFillings = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsFilling == true && x.CartDetailsIDf == Item.CartDetailsID).FirstOrDefault();
                            if (SingleFillings != null)
                                AdditionalItem.ProductFillingDesc = SingleFillings.ProductName;
                            var SingleSauce = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsSauce == true && x.CartDetailsIDf == Item.CartDetailsID).FirstOrDefault();
                            if (SingleSauce != null)
                                AdditionalItem.ProductSauceDesc = SingleSauce.ProductName;
                            //var SingleAddition = AdditionalItems.Where(x => x.ProductIDf == Product.ProductID && x.IsAddition == true && x.CartDetailsIDf == Item.CartDetailsID).ToList();
                            //AdditionalItem.ProductAdditionDescription = new List<ProductAdditionalDescModel>();
                            //foreach (var AdditionObj in SingleAddition)
                            //{
                            //    var AdditionCombostring = "";
                            //    var Addition = Db.ProductAdditionItemMapTbls.Where(x => x.ProductIDf == Product.ProductID && x.ProductAdditionItemIDf == AdditionObj.CartAdditionItemIDf).FirstOrDefault();
                            //    if (Addition != null)
                            //    {
                            //        if (Addition.IsThisFreeItem == true)
                            //        {
                            //            int Count = 1;

                            //            string FreeQty = Convert.ToInt32(AdditionObj.FreeQty).ToString();
                            //            AdditionCombostring += AdditionObj.ProductName + "(" + FreeQty + ")";
                            //            if (Count != SingleAddition.Count())
                            //            {
                            //                AdditionCombostring += ", ";
                            //            }

                            //            Count++;
                            //        }
                            //    }
                            //    ProductAdditionalDescModel NewObj = new ProductAdditionalDescModel();
                            //    NewObj.ProductAdditionDesc = AdditionCombostring;
                            //    AdditionalItem.ProductAdditionDescription.Add(NewObj);
                            //}
                            Item.ComboMealsProductInfo.Add(AdditionalItem);
                        }


                    }



                    //var Additionstring = "";
                    //var FreeAdditions = Db.ProdcutAdditionItemViews.Where(x => x.ProductID == Item.ProductIDf).ToList();

                    //if (FreeAdditions != null && FreeAdditions.Count != 0)
                    //{
                    //    var FreeListOfAdditions = FreeAdditions.Where(x => x.IsThisFreeItem == true).ToList();

                    //    int Count = 1;
                    //    foreach (var addtion in FreeListOfAdditions)
                    //    {
                    //        string FreeQty = Convert.ToInt32(addtion.FreeQty).ToString();
                    //        Additionstring += addtion.ProductAdditionItemName + "(" + FreeQty + ")";
                    //        if (Count != FreeAdditions.Count())
                    //        {
                    //            Additionstring += ", ";
                    //        }

                    //        Count++;
                    //    }

                    //}

                    //var FreeFillings = Db.ProdcutFillingViews.Where(x => x.ProductID == Item.ProductIDf).ToList();


                    //if (FreeFillings != null && FreeFillings.Count != 0)
                    //{
                    //    int Count = 1;
                    //    foreach (var filling in FreeFillings)
                    //    {
                    //        if (Additionstring != "")
                    //        {
                    //            Additionstring += ", ";
                    //        }

                    //        string FreeQty = Convert.ToInt32(filling.FreeQty).ToString();
                    //        Additionstring += filling.ProductFillingItemName + "(" + FreeQty + ")";

                    //        if (Count != FreeFillings.Count())
                    //        {
                    //            Additionstring += ", ";
                    //        }

                    //        Count++;
                    //    }

                    //}

                    //Item.ProductAdditionDesc = Additionstring;


                    ////TotalTaxAmount += Item.SubTotalTaxAmount;
                    //NetAmount += Item.SubNetAmount;
                }
                //if (CartDetailsInfoData.CartTblInfo.TotalOfferAmount != null)
                //{
                //    OfferTotal = CartDetailsInfoData.CartTblInfo.TotalOfferAmount;
                //}


                decimal? NetWithAdditions = NetAmount + TotalAdditionAmount;
                //if (CartDetailsInfoData.ProductOfferTblInfo != null)
                //{
                //    if (CartDetailsInfoData.ProductOfferTblInfo.OfferAmount != null || CartDetailsInfoData.ProductOfferTblInfo.OfferPercentage != null)
                //    {
                //        if (CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Product" || CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Month" || CartDetailsInfoData.ProductOfferTblInfo.OfferType == "Offer By Week" || CartDetailsInfoData.ProductOfferTblInfo.IsOfferAmountTake == true)
                //        {
                //            OfferTotal = CartDetailsInfoData.ProductOfferTblInfo.OfferAmount;
                //        }
                //        else
                //        {
                //            OfferTotal = (NetWithAdditions * CartDetailsInfoData.ProductOfferTblInfo.OfferPercentage) / 100;
                //        }
                //    }
                //}
               
                decimal TaxCalc = 0;
                decimal? CartOfferAmount = OfferTotal;
                decimal? NetAfterDiscount = NetWithAdditions - CartOfferAmount;

                decimal? RestaurantTaxRate = CartDetailsInfoData.RestaurantSettings.TaxPercentage;
                // TaxCalc = Decimal.Parse(((NetAfterDiscount * RestaurantTaxRate) / 100).ToString());
                TaxCalc = Math.Round((((Decimal.Parse(NetWithAdditions.ToString())) - ((Decimal.Parse(NetWithAdditions.ToString())) * (100 / (100 + ((Decimal.Parse(RestaurantTaxRate.ToString())))))))), 2);
                TaxCalc = Math.Round(TaxCalc, 2);


                //decimal? NetAfterTax = NetAfterDiscount + TaxCalc;

                CartDetailsInfoData.CartTblInfo.TotalAmount = TotalAmount;


                CartDetailsInfoData.CartTblInfo.TotalTaxAmount = TaxCalc;

                CartDetailsInfoData.CartTblInfo.TotalDiscountAmount = Math.Round(Decimal.Parse(OfferTotal.ToString()), 2);
                CartDetailsInfoData.CartTblInfo.TotalAdditionsAmount = TotalAdditionAmount;

                CartDetailsInfoData.CartTblInfo.DeliveryCharge = CartDetailsInfoData.CartTblInfo.DeliveryCharge ?? 0;

                decimal? NetAfterDiscountWithDeliveryCharge = NetAfterDiscount + CartDetailsInfoData.CartTblInfo.DeliveryCharge;
                CartDetailsInfoData.CartTblInfo.NetAmount = Math.Round(Decimal.Parse(NetAfterDiscountWithDeliveryCharge.ToString()), 2);

                //UpdateCart Tbl
                var UpdateCart = Db.CartTbls.Where(x => x.CartID == CartID).FirstOrDefault();
                UpdateCart.TotalAmount = TotalAmount ?? 0;
                UpdateCart.TotalTaxAmount = TaxCalc;
                UpdateCart.TotalDiscountAmount = OfferTotal;
                UpdateCart.TotalAdditionsAmount = TotalAdditionAmount;
                UpdateCart.NetAmount = NetAfterDiscount;
                Db.SaveChanges();

                //CartDetailsInfoData.CartAdditionDetailsInfo = CartDetailsInfoData.CartAdditionDetailsInfo.Where(x => x.IsAddition == true).ToList();


                return CartDetailsInfoData;
            }
        }


    }
    public class CartDetailsInfo
    {
        public CartTbl CartTblInfo = new CartTbl();
        public RestaurantSettingsTbl RestaurantSettings = new RestaurantSettingsTbl();
        public List<CartDetails> CartDetailsTblInfo = new List<CartDetails>();
        public OfferDetails ProductOfferTblInfo = new OfferDetails();
        public List<CartAdditionDetails> CartAdditionDetailsInfo = new List<CartAdditionDetails>();
        public List<ProdcutAdditionItemView> ProdcutAdditionItemInfo = new List<ProdcutAdditionItemView>();
        public List<ProdcutAdditionItemView> ProdcutSauceInfo = new List<ProdcutAdditionItemView>();
        public List<ProdcutFillingView> ProdcutFillingInfo = new List<ProdcutFillingView>();
        public List<CartUpgradeMealsInfo> CartUpgradeMealsInfo = new List<CartUpgradeMealsInfo>();
        public BranchAreaTbl BranchAreaDeliveryChargeInfo { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public List<ComboDetailTbl> ComboDetailsInfo = new List<ComboDetailTbl>();
    }
    public class CartUpgradeMealsInfo : CartUpdateMealsItemTbl
    {
        public string UpdateMealItemName { get; set; }
    }
    public class CartDetails : CartDetailsTbl
    {
        public string ProductUpdatemealDesc { get; set; }

        public string ProductAdditionDesc { get; set; }
        public string ProductSauceDesc { get; set; }
        public string ProductFillingDesc { get; set; }
        public string ProductCategoryName { get; set; }
        public string ProductCategoryImage { get; set; }
        public string ProductDescription { get; set; }
        public string TaxGroupName { get; set; }
        public string ProductImage { get; set; }
        public List<CartAdditionDetails> SingleProdcutAdditionInfo = new List<CartAdditionDetails>();
        public List<CartAdditionDetails> SingleProdcutFillingInfo = new List<CartAdditionDetails>();
        public List<CartAdditionDetails> SingleProdcutSauceInfo = new List<CartAdditionDetails>();
        public List<CartUpgradeMealsInfo> SingleCartUpdatemealsInfo = new List<CartUpgradeMealsInfo>();
        public List<ComboMealsCartDetailsModel> ComboMealsProductInfo = new List<ComboMealsCartDetailsModel>();
        public bool  IsComboMeal { get; set; }
    }
    public class ComboMealsCartDetailsModel
    {
        public int? SubProductID { get; set; }
        public string SubProductName { get; set; }
        public List<ProductAdditionalDescModel> ProductAdditionDescription { get; set; }
        public string ProductSauceDesc { get; set; }
        public string ProductFillingDesc { get; set; }
        public List<CartAdditionDetails> SingleProdcutAdditionInfo = new List<CartAdditionDetails>();
        public List<CartAdditionDetails> SingleProdcutFillingInfo = new List<CartAdditionDetails>();
        public List<CartAdditionDetails> SingleProdcutSauceInfo = new List<CartAdditionDetails>();
    }
    public class ProductAdditionalDescModel
    {
        public string ProductAdditionDesc { get; set; }
    }
    public class OfferDetails : ProductOfferTbl
    {
        public int CartID { get; set; }
        public string OfferDesc { get; set; }
    }
    public class CartAdditionDetails : CartAdditionItemsTbl
    {
        public string ProductName { get; set; }


    }


}