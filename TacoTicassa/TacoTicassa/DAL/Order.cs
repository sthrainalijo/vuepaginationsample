﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TacoTicassa.Models;
using static TacoTicassa.Models.ViewModel;

namespace TacoTicassa.DAL
{
    public class Order
    {
        public UserCartDetailsInfo GetOrderDetailsInfo(int? OrderID)
        {
            string ConnectionString = Common.GetConnectionString();
            UserCartDetailsInfo OrderDetailsInfoData = new UserCartDetailsInfo();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("OrderDetailsInfo", param: new { OrderID = OrderID }, commandType: CommandType.StoredProcedure);


                OrderDetailsInfoData.UserOrderInfo = reader.Read<UserOrderViewModel>().FirstOrDefault();
                OrderDetailsInfoData.UserOrderDetailsInfo = reader.Read<OrderDerails>().ToList();
                OrderDetailsInfoData.ProductOfferTblInfo = reader.Read<OfferDetails>().ToList();
                OrderDetailsInfoData.OrderAdditionDetailsInfo = reader.Read<OrderAdditionInfo>().ToList();
                OrderDetailsInfoData.ProdcutAdditionItemInfo = reader.Read<ProdcutAdditionItemView>().ToList();
                OrderDetailsInfoData.ProdcutFillingInfo = reader.Read<ProdcutFillingView>().ToList();
                OrderDetailsInfoData.RewardOrderInfo = reader.Read<RewardedProductInfo>().FirstOrDefault();
                return OrderDetailsInfoData;
            }
        }
    }
    public class UserCartDetailsInfo
    {
        public UserOrderViewModel UserOrderInfo = new UserOrderViewModel();
        public List<OrderDerails> UserOrderDetailsInfo = new List<OrderDerails>();
        public List<OfferDetails> ProductOfferTblInfo = new List<OfferDetails>();
        public List<OrderAdditionInfo> OrderAdditionDetailsInfo = new List<OrderAdditionInfo>();
        public List<ProdcutAdditionItemView> ProdcutAdditionItemInfo = new List<ProdcutAdditionItemView>();
        public List<ProdcutFillingView> ProdcutFillingInfo = new List<ProdcutFillingView>();
        public RewardedProductInfo RewardOrderInfo = new RewardedProductInfo(); 
    }
    public class OrderAdditionInfo:OrderAdditionItemsTbl
    {
        public string AdditionItemName { get; set; }

    }


    public class UserOrderViewModel : OrderTbl
    {
        public int? TotalCount { get; set; }
        public string FullBranch { get; set; }
        public string CustomerNameTbl { get; set; }
        public string DeliveryBoyName { get; set; }

        public float TrackPercentage { get; set; }

        public string OrderPartDate { get; set; }
        public string OrderDeliverPartDate { get; set; }
        public string OrderCancelledOnPartDate { get; set; }
        public string OrderDispatchedPartDate { get; set; }
        public string AssignedOnPartDate { get; set; }

        public int CustomerDeliveryAddressID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine1Arabic { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine2Arabic { get; set; }
        public string State { get; set; }
        public string StateArabic { get; set; }
        public string City { get; set; }
        public string CityArabic { get; set; }
        public string Pin { get; set; }
        public string LandMark { get; set; }
        public string LandMarkArabic { get; set; }
        public string ContactNumber { get; set; }

        public string BasicAddressLine1 { get; set; }
        public string BillingAddressLine2 { get; set; }
        public string BasicCity { get; set; }
        public string BasicState { get; set; }
        public string BasicLandMark { get; set; }
        public string BasicPin { get; set; }
        public string ContactNo { get; set; }


    }
    public class OrderDerails : OrderDetailsTbl
    {
        public string ProductCategoryName { get; set; }
        public string ProductCategoryImage { get; set; }
        public string ProductDescription { get; set; }
        public string TaxGroupName { get; set; }
        public string ProductImage { get; set; }
        public int? ProductID { get; set; }
    }

}