﻿using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TacoTicassa.DAL;
using TacoTicassa.Models;
using WebErrorLogging.Utilities;
using static TacoTicassa.Models.ViewModel;

namespace TacoTicassa.Controllers
{
    public class HomeController : Controller
    {
        DAL.Product Product = new DAL.Product();
        DAL.Cart Cart = new DAL.Cart();
        DAL.Offer OfferDal = new DAL.Offer();
        TacoTicassaDbEntities Db = new TacoTicassaDbEntities();
        #region User management help files
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private string jsonString;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion
        public ActionResult Index(string Session)
        {


            return View();
        }

       
        public class BranchLocation
        {
            public List<LatLng> LocationList = new List<LatLng>();
            public List<BranchTbl> BranchList = new List<BranchTbl>();
        }
        public class LatLng
        {
            public decimal GoogleMapLatitude { get; set; }
            public decimal GoogleMapLongitude { get; set; }

        }

        //start,end are pagination variables
        public JsonResult GetProductList(int? CategoryID, string SearchKey, int? start, int? end)
        {

            //int? BranchID = Convert.ToInt32(Request.Cookies["LocationID"].Value);


            int BranchAreaID = 34;
            int? BranchID = Db.StateBranchViews.Where(x => x.BranchAreaID == BranchAreaID).Select(x => x.BranchID).FirstOrDefault();

            var Products = Product.GetProductData(BranchID, SearchKey, CategoryID, start, end);
            return Json(Products, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductsCategoryWise(int? start, int? end)
        {
            // int? BranchID = Convert.ToInt32(Request.Cookies["LocationID"].Value);
            int BranchAreaID = 34;
            int? BranchID = Db.StateBranchViews.Where(x => x.BranchAreaID == BranchAreaID).Select(x => x.BranchID).FirstOrDefault();

            var ProductsCategoryWise = Product.GetCategoryProductData(BranchID, "", 0, start, end);
            return Json(ProductsCategoryWise, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductCategoryList()
        {
            int BranchAreaID = 34;
            int? BranchID = Db.StateBranchViews.Where(x => x.BranchAreaID == BranchAreaID).Select(x => x.BranchID).FirstOrDefault();
            string BranchIDString = BranchID.ToString();

            string ConnectionString = Common.GetConnectionString();

            List<ProductCategoryTbl> CategoryList = new List<ProductCategoryTbl>();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {

                var reader = conn.QueryMultiple("ProductCategoryInfo", param: new { BranchID = BranchIDString }, commandType: CommandType.StoredProcedure);

                CategoryList = reader.Read<ProductCategoryTbl>().ToList();

            }
            //var ProductsCategory = Db.ProductCategoryTbls.Where(x => x.IsActive == true).OrderBy(x=>x.ProductCategoryOrder).ToList();
            return Json(CategoryList, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult BranchIDAutoComplete(string term)
        {

            var search = term.Trim();
            //var ItemList = (from items in Db.StateBranchViews.OrderBy(x => x.BranchName).Where(x=>x.BranchName.Contains(term)||x.BranchName.Contains(term)) select new { label = items.StateName.ToUpper() + ' ' + items.BranchName, value = items.BranchID }).Take(20).ToList();
            var ItemList = Db.StateBranchViews.OrderBy(x => x.FullArea).Where(x => x.FullArea.Contains(term) || x.BranchName.Contains(term)).Select(x => new { label = x.FullArea, value = x.BranchAreaID }).ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);

        }
  

        #region Helpers

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Db != null)
                {
                    Db.Dispose();
                    Db = null;
                }

                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
     
    }

    public class TockenResponse
    {
        public string access_token { get; set; }
    }
    public class CreateResponse
    {
        public string _id { get; set; }
        public string _links { get; set; }
    }
    public class Amount
    {
        public int netamount { get; set; }
    }

}