//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    
    public partial class SearchBranchArea_Result
    {
        public Nullable<int> TotalCount { get; set; }
        public int BranchAreaID { get; set; }
        public string AreaName { get; set; }
        public int BranchID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public decimal DeliveryCharge { get; set; }
        public string ParentBranch { get; set; }
    }
}
