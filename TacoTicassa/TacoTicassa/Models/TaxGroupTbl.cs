//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaxGroupTbl
    {
        public int TaxGroupID { get; set; }
        public Nullable<int> TaxGroupCode { get; set; }
        public string TaxGroupName { get; set; }
        public string TaxGroupNameArabic { get; set; }
        public Nullable<decimal> TaxPercentage { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedByUserID { get; set; }
        public Nullable<int> CreatedByBranchID { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
