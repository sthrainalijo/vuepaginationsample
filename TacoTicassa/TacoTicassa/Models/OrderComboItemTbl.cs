//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrderComboItemTbl
    {
        public int OrderComboItemID { get; set; }
        public Nullable<int> ComboDetailID { get; set; }
        public Nullable<int> OrderIDf { get; set; }
        public string ProductName { get; set; }
        public Nullable<int> OrderDetailID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public Nullable<int> ComboProductID { get; set; }
    }
}
