//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RewardsDetailsTbl
    {
        public int RewardsDetailId { get; set; }
        public Nullable<int> RewardsId { get; set; }
        public Nullable<int> RewardItemId { get; set; }
        public Nullable<int> BranchIDf { get; set; }
    }
}
