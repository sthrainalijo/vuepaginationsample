//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LoyaltyCardDetailTbl
    {
        public int LoyaltyCardDetailId { get; set; }
        public Nullable<int> LoyaltyCardIDF { get; set; }
        public Nullable<int> RewardLimitToClaimOffer { get; set; }
        public Nullable<int> ProductToClaim { get; set; }
        public Nullable<decimal> MinimumAmountToGetReward { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedOn { get; set; }
    }
}
