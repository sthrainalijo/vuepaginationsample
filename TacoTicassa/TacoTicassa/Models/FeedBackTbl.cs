//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeedBackTbl
    {
        public int FeedBackID { get; set; }
        public Nullable<int> FeedBackRate { get; set; }
        public string CreatedByUserID { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
