﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TacoTicassa.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public string FullName { get; set; }
        public string FullNameArabic { get; set; }

        public string ContactNo { get; set; }
        
        public string BillingAddressLine1 { get; set; }
        public string BillingAddressLine1Arabic { get; set; }
        public string BillingAddressState { get; set; }
        public string BillingAddressCity { get; set; }
        public string BillingAddressPin { get; set; }
        public string BillingAddressLandMark { get; set; }

        
        public Nullable<int> BranchID { get; set; }

        public Nullable<int> UserCode { get; set; }
        public string UserCodeArabic { get; set; }
        
        public string UserType { get; set; }

        public string CreatedByUserID { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedByBranchID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> IsDeliveryAddressSame { get; set; }
        public string ProfileImage { get; set; }
        public string BillingAddressLine2 { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> DateOfBirth { get; set; }

        public string Nationality { get; set; }
        public Nullable<bool> IsFirstTimeRegisteration { get; set; }
        public Nullable<bool> IsGuestUser { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public Nullable<bool> IsNumberVerified { get; set; }
        public string RegisterUserCoupon { get; set; }



        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}