//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class NotificationTbl
    {
        public int NotificationID { get; set; }
        public string NotificationTitle { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public Nullable<bool> IsNotified { get; set; }
        public Nullable<bool> IsBooked { get; set; }
        public Nullable<bool> IsOrdered { get; set; }
        public Nullable<int> OrderID { get; set; }
    }
}
