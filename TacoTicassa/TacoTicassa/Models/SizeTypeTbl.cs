//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SizeTypeTbl
    {
        public int SizeTypeID { get; set; }
        public string SizeType { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
