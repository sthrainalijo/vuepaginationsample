﻿using Foolproof;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace TacoTicassa.Models
{
    public class OfferViewModel
    {
        #region Product Offer
        public class ProductOfferViewModel
        {
            public int? ProductOfferID { get; set; }

            public int? ProductOfferCode { get; set; }

            [Required(ErrorMessage = "Please Enter Offer Name")]
            [Display(Name = "Offer Name")]
            public string OfferName { get; set; }

            [Display(Name = "Offer Name in Arabic")]
            public string OfferNameArabic { get; set; }

            [Required(ErrorMessage = "Please Select Offer Type")]
            [Display(Name = "Offer Type")]
            public string OfferType { get; set; }

            [RequiredIf("OfferType", "Offer By Product", ErrorMessage = "Product Name is required")]
            public int? ProductIDf { get; set; }


            [Display(Name = "Product Name")]
            public string ProductName { get; set; }

            public decimal? ProductPrice { get; set; }

            public int? CreatedByBranchID { get; set; }
            [Display(Name = "Offer Percent")]
            //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid Price")]
            
           //[RequiredIf("OfferInPorA",true, ErrorMessage = "*** Enter either Offer In % (or) Offer In Amount.This will deduct from the bill amount on coupon code apply")]
            public decimal? OfferPercent { get; set; }


            [Display(Name = "Offer Amount")]
            //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid Price")]
            public decimal? OfferAmount { get; set; }


            [Display(Name = "Offer Quantity")]
            public int? OfferQty { get; set; }


            [RequiredIf("OfferType", "Offer By Month", ErrorMessage = "Offer Month Date is required")]
            [Display(Name = "Offer Month Date")]
            public string OfferMonthDate { get; set; }

            [RequiredIf("OfferType", "Offer By Week", ErrorMessage = "Offer Week Day is required")]
            [Display(Name = "Offer Week Day")]
            public string OfferWeek{ get; set; }


            [RequiredIf("OfferType", "Offer By Date", ErrorMessage = "Offer Date is required")]
            [Display(Name = "Offer Date")]
            public string OfferDate { get; set; }

            //[RequiredIf("DateChecker",true, ErrorMessage = ("Start date must be less that end date"))]
            [Display(Name = "Offer Start Date")]
            public string OfferStartDate { get; set; }

            [Display(Name = "Offer End Date")]
            public string OfferEndDate { get; set; }

            //[RequiredIf("TimeChecker",true, ErrorMessage = ("Start time must be less that end time"))]
            [Display(Name = "Offer Start Time")]
            public string OfferStartTime { get; set; }


            [Display(Name = "Offer End Time")]
            public string OfferEndTime { get; set; }

            //public List<ItemViewModel> ItemList = new List<ItemViewModel>(); 

            [Display(Name = "Offer Description")]
            public string OfferDesc { get; set; }

            [Display(Name = "Offer Image")]
            public string OfferBannerImage { get; set; }

            public string Option { get; set; }

            public string OfferUserId { get; set; }

            //[RequiredIf("FirstOrSingleUser", true, ErrorMessage = ("Please select atleast one user or select checkbox for first order"))]
            public string OfferUserName { get; set; }
            public string IsthisOfferForUserFirstOrder { get; set; }
            public string IsOfferAmountTake { get; set; }

            public int? OfferUsageCount { get; set; }
            public string IsAllBranch { get; set; }


            public decimal? MinimumCartAmount { get; set; }

            //[NotMapped]
            //public bool FirstOrSingleUser
            //{
            //    get
            //    {
            //        if (IsthisOfferForUserFirstOrder != "on" && (OfferUserName != null || OfferUserName != "") && OfferType=="Offer By User")
            //        {
            //            return true;
            //        }
            //        else
            //        {
            //            return false;
            //        }
            //    }
            //}
            //[NotMapped]
            //public bool OfferInPorA
            //{
            //    get
            //    {
            //        if (OfferPercent == null && OfferAmount == null)
            //        {
            //            return true;
            //        }
            //        else
            //        {
            //            return false;
            //        }
            //    }
            //}

            //[NotMapped]
            //public bool DateChecker
            //{
            //    get
            //    {
            //        if (OfferStartDate != null)
            //        {
            //            var FormatedStartDate = DateTime.ParseExact(OfferStartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //            var FormatedEndDate = DateTime.ParseExact(OfferEndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //            if (FormatedStartDate.Date >= FormatedEndDate.Date)
            //            {
            //                return true;
            //            }
            //            else
            //            {
            //                return false;
            //            }
            //        }
            //        else if ((OfferStartDate == null || OfferStartDate == "") && OfferType == "Offer By Season")
            //        {
            //            return true;
            //        }
            //        else
            //        {
            //            return false;
            //        }

            //    }
            //}
            ////[NotMapped]
            ////public bool EndDateChecker
            ////{
            ////    get
            ////    {
            ////        if (OfferEndDate != null)
            ////        {
            ////            var FormatedStartDate = DateTime.ParseExact(OfferStartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            ////            var FormatedEndDate = DateTime.ParseExact(OfferEndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            ////            if (FormatedEndDate <= FormatedStartDate)
            ////            {
            ////                return true;
            ////            }
            ////            else
            ////            {
            ////                return false;
            ////            }
            ////        }
            ////        else if ((OfferEndDate == null || OfferEndDate == "") && OfferType == "Offer By Season")
            ////        {
            ////            return true;
            ////        }
            ////        else
            ////        {
            ////            return false;
            ////        }
            ////    }
            ////}
            //[NotMapped]
            //public bool TimeChecker
            //{
            //    get
            //    {
            //        if (OfferStartTime != null)
            //        {
            //            var FormatedStartTime = DateTime.ParseExact(OfferStartTime, "HH:mm", CultureInfo.InvariantCulture);
            //            var FormatedEndTime = DateTime.ParseExact(OfferEndTime, "HH:mm", CultureInfo.InvariantCulture);
            //            if (FormatedStartTime >= FormatedEndTime)
            //            {
            //                return true;
            //            }
            //            else
            //            {
            //                return false;
            //            }

            //        }
            //        else if ((OfferStartTime == null || OfferStartTime == "") && OfferType == "Offer By Hour")
            //        {
            //            return true;
            //        }
            //        else
            //        {
            //            return false;
            //        }
            //    }
            //}

            ////[NotMapped]
            ////public bool EndTimeChecker
            ////{
            ////    get
            ////    {
            ////        if (OfferEndTime != null)
            ////        {
            ////            var FormatedStartTime = DateTime.ParseExact(OfferStartTime, "HH:mm", CultureInfo.InvariantCulture);
            ////            var FormatedEndTime = DateTime.ParseExact(OfferEndTime, "HH:mm", CultureInfo.InvariantCulture);
            ////            if (FormatedEndTime <= FormatedStartTime)
            ////            {
            ////                return true;
            ////            }
            ////            else
            ////            {
            ////                return false;
            ////            }
            ////        }
            ////        else if ((OfferEndTime == null || OfferEndTime == "") && OfferType == "Offer By Hour")
            ////        {
            ////            return true;
            ////        }
            ////        else
            ////        {
            ////            return false;
            ////        }
            ////    }
            ////}


        }


        public class ItemViewModel
        {
            public int? ItemID { get; set; }

            public string ItemName { get; set; }
        }
        #endregion

    }
}