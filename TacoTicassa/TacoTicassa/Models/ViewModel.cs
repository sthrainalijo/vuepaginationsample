﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TacoTicassa.Models
{


    public class ViewModel
    {
        public static class HelperMethods
        {
            public static string GetRoleName()
            {
                return (string)HttpContext.Current.Session["RoleName"];
            }

            public static void SetRoleName(string RoleName)
            {
                HttpContext.Current.Session["RoleName"] = RoleName;
            }
            public static int? GetBranchID()
            {
                return (int?)HttpContext.Current.Session["BranchID"];
            }

            public static void SetBranchID(int? BranchID)
            {
                HttpContext.Current.Session["BranchID"] = BranchID;
            }
        }
        public class LoginInfoModel : AspNetUser
        {
            public string RoleName { get; set; }
            public string UserID { get; set; }
        }

        #region PromotionViewModel
        public class PromotionViewModel
        {
            public string MailSubject { get; set; }
            public string TemplateName { get; set; }
            public int? TemplateIDf { get; set; }
            public int CreatedByBranchID { get; set; }

            [AllowHtml]
            public string EmailContent { get; set; }

        }
        #endregion

        #region Branch
        public class BranchViewModel
        {

            public int BranchID { get; set; }

            [Required(ErrorMessage = "Please Enter Branch Name")]
            [Display(Name = "Branch Name")]
            public string BranchName { get; set; }


            [Display(Name = "Branch Name Arabic")]
            public string BranchNameArabic { get; set; }

            [Required(ErrorMessage = "Please Enter Branch Invoice Prefix")]
            [Display(Name = "Branch Invoice Prefix")]
            public string BranchInvoicePrefix { get; set; }

            [EmailAddress]
            [Required(ErrorMessage = "Please Enter Branch Email")]
            [Display(Name = "Branch Email")]
            public string BranchEmail { get; set; }


            public string PlaceName { get; set; }

            [Required(ErrorMessage = "Please Enter Place of Branch")]
            [Display(Name = "Branch Place")]
            public int? PlaceIDf { get; set; }

            [Display(Name = "Branch Address")]
            public string BranchAddress { get; set; }

            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string CenterPhoneNumber { get; set; }

            [Display(Name = "Toll Free Number ")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Toll Free Number must be numeric")]
            public string TollFreeNumber { get; set; }

            [Display(Name = "Parent Branch")]
            public int? ParentBranchIDf { get; set; }

            public string GoogleMapLatitude { get; set; }
            public string GoogleMapLongitude { get; set; }
            public string GoogleMapAccuracy { get; set; }
            public string GoogleMapAddress { get; set; }
            public string Location { get; set; }

            public string Option { get; set; }
            public string Type { get; set; }
        }

        public class BranchAreaViewModel
        {

            public int BranchAreaID { get; set; }

            [Required(ErrorMessage = "Please Enter Branch Area Name")]
            [Display(Name = "Branch Area Name")]
            public string BranchAreaName { get; set; }

            [Display(Name = "Parent Branch")]
            public int ParentBranchIDf { get; set; }

            public string Option { get; set; }
            [Display(Name = "Delivery Charge")]
            public decimal DeliveryCharge { get; set; }

        }


        #endregion

        #region Branch Admin

        public class BranchAdminViewModel
        {
            public string UserID { get; set; }

            [Display(Name = "Admin Name")]
            [Required(ErrorMessage = "Please Enter Admin Name")]
            public string AdminName { get; set; }

            [Display(Name = "Admin Name in Arabic")]
            public string AdminNameArabic { get; set; }

            [Display(Name = "Select Branch")]
            [Required(ErrorMessage = "Please Select a Branch")]
            public int? BranchIDf { get; set; }

            public string SearchBranch { get; set; }

            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string ContactNumber { get; set; }

            [EmailAddress]
            [Required(ErrorMessage = "Please Enter Admin Email")]
            [Display(Name = "Admin Email")]
            public string AdminEmail { get; set; }


            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string Option { get; set; }
        }
        #endregion
        #region Supervisor

        public class SupervisorViewModel
        {
            public string UserID { get; set; }

            [Display(Name = "Supervisor Name")]
            [Required(ErrorMessage = "Please Enter Supervisor Name")]
            public string SupervisorName { get; set; }

            [Display(Name = "Supervisor Name in Arabic")]
            public string SupervisorNameArabic { get; set; }

            [Display(Name = "Select Branch")]
            [Required(ErrorMessage = "Please Select a Branch")]
            public int? BranchIDf { get; set; }

            public string SearchBranch { get; set; }

            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string ContactNumber { get; set; }

            [EmailAddress]
            [Required(ErrorMessage = "Please Enter Supervisor Email")]
            [Display(Name = "Supervisor Email")]
            public string SupervisorEmail { get; set; }


            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string Option { get; set; }
        }
        #endregion

        #region Branch Supervisor

        public class BranchSupervisorViewModel
        {
            public string UserID { get; set; }

            [Display(Name = "Supervisor Name")]
            [Required(ErrorMessage = "Please Enter Supervisor Name")]
            public string SupervisorName { get; set; }

            [Display(Name = "Supervisor Name in Arabic")]
            public string SupervisorNameArabic { get; set; }

            [Display(Name = "Select Branch")]
            [Required(ErrorMessage = "Please Select a Branch")]
            public int? BranchIDf { get; set; }

            public string SearchBranch { get; set; }

            [Display(Name = "Contact Number")]
            [Required(ErrorMessage = "Phone Number Required!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string ContactNumber { get; set; }

            [EmailAddress]
            [Required(ErrorMessage = "Please Enter Supervisor Email")]
            [Display(Name = "Supervisor Email")]
            public string SupervisorEmail { get; set; }


            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string Option { get; set; }
        }
        #endregion

        #region TaxGroup
        public class TaxGroupViewModel
        {

            public int TaxGroupID { get; set; }

            [Required(ErrorMessage = "Please Enter Tax Group Name")]
            [Display(Name = "Tax Group Name")]
            public string TaxGroupName { get; set; }


            [Display(Name = "Tax Group Name Arabic")]
            public string TaxGroupNameArabic { get; set; }

            [Required(ErrorMessage = "Please Enter Tax Amount")]
            [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Tax Amount must be numeric or decimal")]
            [Display(Name = "Tax Amount")]
            public decimal? TaxAmount { get; set; }



            public string Option { get; set; }
            public string Type { get; set; }
        }

        #endregion

        #region OfferType
        public class OfferTypeViewModel
        {

            public int ProductOfferTypeID { get; set; }

            [Required(ErrorMessage = "Please Enter Offer Type Name")]
            [Display(Name = "Offer Type  Name")]
            public string OfferTypeName { get; set; }


            [Display(Name = "Offer Type Name Arabic")]
            public string OfferTypeNameArabic { get; set; }

            public string Option { get; set; }
            public string Type { get; set; }
        }

        #endregion

        #region RestaurantSettings
        public class RestaurantSettingsViewModel
        {

            public int RestaurantSettingsID { get; set; }

            [Required(ErrorMessage = "Please Enter Restaurant Name")]
            [Display(Name = "Restaurant Name")]
            public string RestaurantName { get; set; }

            [Display(Name = "Restaurant Name Arabic")]
            public string RestaurantNameArabic { get; set; }

            [Display(Name = "Restaurant Address Line1")]
            public string RestaurantAddressLine1 { get; set; }

            [Display(Name = "Restaurant Address Line1  Arabic")]
            public string RestaurantAddressLine1Arabic { get; set; }

            [Display(Name = "Restaurant Address Line2")]
            public string RestaurantAddressLine2 { get; set; }

            [Display(Name = "Restaurant Address Line2  Arabic")]
            public string RestaurantAddressLine2Arabic { get; set; }

            [Display(Name = "State")]
            public string State { get; set; }
            [Display(Name = "City")]
            public string City { get; set; }
            [Display(Name = "Pin")]
            public int? Pin { get; set; }
            [Display(Name = "Registeration Number")]
            public string RegisterationNumber { get; set; }

            [Display(Name = "Sales Invoice Prefix")]
            public string SalesInvoicePrefix { get; set; }


            [Display(Name = "Tax Percentage")]
            public decimal? TaxPercentage { get; set; }

            [Display(Name = "Restaurant Logo")]
            public HttpPostedFileBase RestaurantLogoFile { get; set; }

            public string RestaurantLogo { get; set; }

            [Display(Name = "Delivery Charge")]
            public decimal? DeliveryCharge { get; set; }

            [Display(Name = "Minimum Amount Of Delivery")]
            public decimal? MinimumAmountOfDelivery { get; set; }


            [Display(Name = "Update Meal Charge")]
            public decimal? UpdateMealCharge { get; set; }

            public string Option { get; set; }
            public string Type { get; set; }
        }

        #endregion

        #region SubProductViewModel
        public class SubProductViewModel : MapSubProductTbl
        {
            public string ProductName { get; set; }
            public string IsOptionalRadio { get; set; }
        }
        #endregion
        #region Product

        public class ProductViewModel
        {
            public int ProductID { get; set; }
            public int? VariantIDTemp { get; set; }
            public int? AdditionIDTemp { get; set; }
            public int? FillingIDTemp { get; set; }
            public int? SubProductIdTemp { get; set; }

            public string ProductCode { get; set; }


            public int? ProductOrder { get; set; }

            [Required(ErrorMessage = "Please Enter Product Name")]
            [Display(Name = "Product Name")]
            public string ProductName { get; set; }

            [Display(Name = "Product Name in Arabic")]
            public string ProductNameArabic { get; set; }

            [Required(ErrorMessage = "Please Enter Product Price")]
            [Display(Name = "Product Price")]
            [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid Price")]
            public decimal? ProductAmount { get; set; }

            [Display(Name = "Tax Group")]
            public int? TaxGroupIDf { get; set; }

            [Display(Name = "Product Category")]
            public string ProductCategoryName { get; set; }

            [Required(ErrorMessage = "Please Select Product Category")]
            public int? ProductCategoryIDf { get; set; }

            // [Required(ErrorMessage = "Please Upload Product Image")]
            [Display(Name = "Browse Product Image")]
            public string ProductImage { get; set; }

            [StringLength(150, MinimumLength = 3, ErrorMessage = "Description Length Limited Upto 150 Characters")]
            [Display(Name = "Product Description")]
            public string ProductDescription { get; set; }


            [Display(Name = "Product Description in Arabic")]
            public string ProductDescriptionArabic { get; set; }

            public DateTime? CreatedOn { get; set; }

            public string CreatedByUserID { get; set; }

            public int? CreatedByBranchID { get; set; }
            public string IsProductAdditionItem { get; set; }
            public string IsProductFillingItem { get; set; }
            public string IsThisSauce { get; set; }
            public string IsUpdateToMealAllowed { get; set; }

            public string IsStockCompleted { get; set; }
            public string IsAllBranch { get; set; }


            public bool? IsActive { get; set; }

            public string Option { get; set; }

            public List<ProductTbl> ProductAdditionsList = new List<ProductTbl>();
            public List<ProductTbl> ProductSizeTypeList = new List<ProductTbl>();
            public List<ProductTbl> ProductFillingList = new List<ProductTbl>();
            public List<ProductTbl> ProductSauceList = new List<ProductTbl>();


            public List<VariantTbl> VariantsList = new List<VariantTbl>();
            public List<SubProductViewModel> MappingSubProductList = new List<SubProductViewModel>();
            public List<ProductUpdateMealsItemTbl> ExistingUpdateDrinkItemList = new List<ProductUpdateMealsItemTbl>();
            public List<ProductUpdateMealsItemTbl> ExistingUpdateDishItemList = new List<ProductUpdateMealsItemTbl>();
            public List<ProdcutAdditionItemView> ExistingAdditions = new List<ProdcutAdditionItemView>();
            public List<ProdcutFillingView> ExistingFillings = new List<ProdcutFillingView>();
            public List<ProdcutFillingView> ExistingSizes = new List<ProdcutFillingView>();
            public List<ProductVariantTbl> ExistingVariants = new List<ProductVariantTbl>();
            public List<ProdcutSauceView> ExistingSauces = new List<ProdcutSauceView>();
        }
        public class AdditionsCheckList
        {
            public int ProductAdditionItemID { get; set; }
            public string CheckAddition { get; set; }
        }

        public class FillingsCheckList
        {
            public int ProductFillingItemID { get; set; }
            public string CheckFillings { get; set; }
        }
        public class SauceCheckList
        {
            public int ProductSauceItemID { get; set; }
            public string CheckSauce { get; set; }
        }
        #endregion

        #region Product Category

        public class ProductCategoryViewModel
        {
            public int ProductCategoryID { get; set; }

            [Display(Name = "Product Category Code")]
            public string ProductCategoryCode { get; set; }

            [Required(ErrorMessage = "Please Enter Category Name")]
            [Display(Name = "Product Category Name")]
            public string ProductCategoryName { get; set; }

            [Display(Name = "Product Category Name in Arabic")]
            public string ProductCategoryNameArabic { get; set; }

            //[Required(ErrorMessage = "Please Upload Product Category Image")]
            [Display(Name = "Browse Product Category Image")]
            public string ProductCategoryImage { get; set; }

            [Display(Name = "Product Category Description")]
            public string ProductCategoryDesc { get; set; }

            [Display(Name = "Product Category Description in Arabic")]
            public string ProductCategoryDescArabic { get; set; }

            public DateTime? CreatedOn { get; set; }

            public string CreatedByUserID { get; set; }

            public int CreatedByBranchID { get; set; }
            public string SearchBranchID { get; set; }

            public bool? IsActive { get; set; }

            public string Option { get; set; }

            public string IsAllBranch { get; set; }
            public List<BranchTbl> BranchList = new List<BranchTbl>();
        }

        #endregion

        #region Deliver Boy

        public class DeliveryBoyViewModel
        {
            public string UserID { get; set; }

            [Required(ErrorMessage = "Please Enter Full Name")]
            [Display(Name = "Full Name")]
            public string FullName { get; set; }

            [Required(ErrorMessage = "Please Enter Contact Number")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            [Display(Name = "Contact Number")]
            public string ContactNumber { get; set; }

            [Display(Name = "Branch")]
            public string SearchBranch { get; set; }

            [Display(Name = "Branch")]
            public int? BranchIDf { get; set; }

            //[Required(ErrorMessage = "Please Enter Email")]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            //[Required(ErrorMessage = "Please Enter Password")]
            //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            //[DataType(DataType.Password)]
            //[Display(Name = "Password")]
            //public string Password { get; set; }

            //[DataType(DataType.Password)]
            //[Display(Name = "Confirm password")]
            //[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            //public string ConfirmPassword { get; set; }

            public string Option { get; set; }
        }

        #endregion

        #region Profile Update

        public class ProfileViewModel
        {
            public string UserID { get; set; }

            public string Name { get; set; }

            public string NameArabic { get; set; }

            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string ContactNumber { get; set; }

            [EmailAddress]
            public string Email { get; set; }

            [Display(Name = "Browse Profile Image")]
            public string ProfileImage { get; set; }

            public BranchTbl BranchInfo = new BranchTbl();


        }

        #endregion

        #region Order
        public class OrderAssignViewModel
        {
            public int OrderID { get; set; }

            [Required(ErrorMessage = "Please Select Delivery Boy")]
            [Display(Name = "Delivery Boy")]
            public string DeliveryBoyIDf { get; set; }
            public string SearchDelivery { get; set; }

            public string AssigneeNote { get; set; }

        }
        public class OrderDeliverViewModel
        {
            public int OrderID { get; set; }

            [Required(ErrorMessage = "Please Select Delivery Boy")]
            [Display(Name = "Delivery Boy")]
            public string DeliveryBoyIDf { get; set; }
            [Required(ErrorMessage = "Please Select Payment Status")]
            public string PaymentStatus { get; set; }

            public string PaymentNote { get; set; }
        }


        public class OrderViewModel
        {
            public int? OrderID { get; set; }
            public string OrderStatus { get; set; }
            public string Option { get; set; }
            public string UserRole { get; set; }
        }
        #endregion

        #region Report
        public class ReportViewModel
        {

            //[Display(Name = "Select Provider")]
            //public string StaffID { get; set; }

            //public string SearchProduct { get; set; }
            [Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            public string OrderFromDate { get; set; }

            [Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            public string OrderToDate { get; set; }

            public int? BranchIDf { get; set; }

            public string SearchBranch { get; set; }

            public string OrderStatus { get; set; }

            public string CustomerIDf { get; set; }

            public string CustomerName { get; set; }

            public int? ProductIDf { get; set; }

            public string ProductName { get; set; }

            public int? BranchAreaIDf { get; set; }

            public string SearchBranchArea { get; set; }

            public decimal? AmountPaid { get; set; }

            public string AllReport { get; set; }

            public string Option { get; set; }

            public List<OrderInfoDataModel> OrderList = new List<OrderInfoDataModel>();
            public List<OrderProductAggregateModel> OrderProductSummary = new List<OrderProductAggregateModel>();

            //[Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            //public string SalesFromDate { get; set; }

            //[Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            //public string SalesToDate { get; set; }

            //[Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            //public string SalesReturnFromDate { get; set; }

            //[Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            //public string SalesReturnToDate { get; set; }

            //[Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            //public string PurchaseReturnFromDate { get; set; }

            //[Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            //public string PurchaseReturnToDate { get; set; }


            //[Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            //public string AppointmentFromDate { get; set; }

            //[Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            //public string AppointmentToDate { get; set; }


            //[Display(Name = "From Date")]
            //[Required(ErrorMessage = "Please select from date")]
            //public string StockFromDate { get; set; }

            //[Display(Name = "To Date")]
            //[Required(ErrorMessage = "Please select to date")]
            //public string StockToDate { get; set; }


            //[Display(Name = "Appointment Status")]
            //public string AppointmentStatus { get; set; }
            //public string Option { get; set; }
        }

        public class OrderInfoDataModel
        {
            public string BranchName { get; set; }

            public string OrderTime { get; set; }
            public string OrderInvoiceNumber { get; set; }
            public decimal? TotalAmount { get; set; }
            public decimal? TotalTaxAmount { get; set; }
            public decimal? TotalOfferAmount { get; set; }
            public decimal? TotalDiscountAmount { get; set; }
            public decimal? NetAmount { get; set; }
            public decimal? TotalQty { get; set; }
            public string PaymentMode { get; set; }
            public string CustomerName { get; set; }
            public string OrderStatus { get; set; }
            public string PaymentStatus { get; set; }
            public string OrderDate { get; set; }
            public string ContactNo { get; set; }
            public string OrderCancelReason { get; set; }
            public string OrderCancelledBy { get; set; }
            public string OrderDispatchedOnPart { get; set; }
            public string AreaName { get; set; }

        }

        public class OrderProductAggregateModel
        {
            public int? ProductID { get; set; }
            public string ProductName { get; set; }
            public decimal? TotalQty { get; set; }
            public decimal? TotalAmount { get; set; }
            public decimal? TotalTaxAmount { get; set; }
            public decimal? TotalOfferAmount { get; set; }
            public decimal? TotalDiscountAmount { get; set; }
            public decimal? NetAmount { get; set; }

        }


        public class LocationReportViewModel
        {


            public int? BranchIDf { get; set; }

            public string SearchBranch { get; set; }

            public int? BranchAreaIDf { get; set; }

            public string SearchBranchArea { get; set; }


            public string CustomerIDf { get; set; }

            public string CustomerName { get; set; }

            public string Nationality { get; set; }

            public string Gender { get; set; }
            public string Age { get; set; }

            public List<CustomerInfoModel> CustomerList = new List<CustomerInfoModel>();

        }
        public class CustomerInfoModel : AspNetUser
        {
            public string DOB { get; set; }
        }
        #endregion

        #region Check Out

        public class CheckOutViewModel
        {
            public string UserID { get; set; }

            [Required(ErrorMessage = "Please Enter Full Name")]
            public string FullName { get; set; }

            [Required(ErrorMessage = "Please Enter Email Address")]
            public string EmailAddress { get; set; }

            [Required(ErrorMessage = "Please Enter Address")]
            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }
            public string RewardItem { get; set; }

            [Required(ErrorMessage = "Please Enter City")]
            public string City { get; set; }

            //[Required(ErrorMessage = "Please Enter Pincode")]
            //public string Pin { get; set; }

            public string Landmark { get; set; }

            [Required(ErrorMessage = "Please Enter Phone Number!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string PhoneNumber { get; set; }

            public string IsSameAddress { get; set; }

            public int? DeliveryAddressIDf { get; set; }

            [Required(ErrorMessage = "Please Enter Address")]
            public string DeliveryAdressLine1 { get; set; }

            public string DeliveryAdressLine2 { get; set; }

            [Required(ErrorMessage = "Please Enter City")]
            public string DeliveryCity { get; set; }

            [Required(ErrorMessage = "Please Enter Pincode")]
            public string DeliveryPin { get; set; }

            public string DeliveryLandmark { get; set; }
            public bool IsGuestUser { get; set; }

            [Required(ErrorMessage = "Please Enter Phone Number!")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string DeliveryPhoneNumber { get; set; }

            [Required(ErrorMessage = "Please Select Payment Type")]
            public string PaymentType { get; set; }

            [Required(ErrorMessage = "Please Select Shipment Type")]
            public string ShipmentType { get; set; }

            public string GoogleMapLatitude { get; set; }
            public string GoogleMapLongitude { get; set; }
            public string GoogleMapAccuracy { get; set; }
            public string GoogleMapAddress { get; set; }


            [Required(ErrorMessage = "Please Select Location!!")]
            public string Location { get; set; }

            public string OrderTime { get; set; }
            public string OrderMinute { get; set; }
            public string OrderHours { get; set; }
            public string IsDeliveryTime { get; set; }
            public bool? IsNumberVerified { get; set; }

            public string Message { get; set; }
            public string Type { get; set; }
            public string ProductsName { get; set; }

            public decimal? MinimumAmountOfDelivery { get; set; }

            public LoyaltyRewardInfoViewModel loyaltyRewardInfo = new LoyaltyRewardInfoViewModel();
        }

        public class LocationViewModel
        {
            public int LocationID { get; set; }
            public string LocationName { get; set; }
        }
        #endregion

        #region User Profile Settings

        public class UserSettingsViewModel
        {

            public string UserID { get; set; }

            [Required]
            [Display(Name = "Full Name")]
            public string FullName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }


            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string UserImage { get; set; }

            [Required(ErrorMessage = "Please Enter Your Date Of Birth")]
            public string DateOfBirth { get; set; }
            public string Nationality { get; set; }
            public string Gender { get; set; }
            public string Age { get; set; }

            [Required(ErrorMessage = "A Valid Contact Number is Mandatory")]
            [RegularExpression("^[0-9]*$", ErrorMessage = "Contact Number must be numeric")]
            public string PhoneNumber { get; set; }

            public string RegisterUserCoupon { get; set; }
        }

        #endregion

        #region WorkingHour
        public class WorkingHourViewModel
        {
            public string SearchBranch { get; set; }
            public string UserType { get; set; }

            public string StartTimeLock { get; set; }

            public int? BranchIDf { get; set; }

            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Monday Starting Time")]
            public string MondayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Monday  Ending Time")]
            public string MondayEnd { get; set; }

            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Tuesday Starting Time")]
            public string TuesdayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Tuesday  Ending Time")]
            public string TuesdayEnd { get; set; }
            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Wednesday Starting Time")]
            public string WednesdayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Wednesday  Ending Time")]
            public string WednesdayEnd { get; set; }
            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Thursday Starting Time")]
            public string ThursdayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Thursday  Ending Time")]
            public string ThursdayEnd { get; set; }
            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Friday Starting Time")]
            public string FridayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Friday  Ending Time")]
            public string FridayEnd { get; set; }
            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Saturday Starting Time")]
            public string SaturdayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Saturday  Ending Time")]
            public string SaturdayEnd { get; set; }
            [Required(ErrorMessage = "Please Enter Starting Time")]
            [Display(Name = "Sunday Starting Time")]
            public string SundayStart { get; set; }

            [Required(ErrorMessage = "Please Enter Ending Time")]
            [Display(Name = "Sunday  Ending Time")]
            public string SundayEnd { get; set; }



            public string Option { get; set; }
            public string Type { get; set; }
        }

        #endregion

        #region Permission Settings
        public class PermissionViewModel
        {
            public int PermissionEmployeeTypeID { get; set; }

            public int PermissionPageID { get; set; }

            [Display(Name = "Select User Type")]
            public int? EmployeeTypeID { get; set; }
            public string Option { get; set; }
            public List<PagePermissionViewModel> PageList = new List<PagePermissionViewModel>();
            public List<PermissionPageTbl> Pages = new List<PermissionPageTbl>();
        }
        public class PagePermissionViewModel
        {
            public int PermissionPageID { get; set; }
            public string PermissionPageName { get; set; }
            public bool IsPermitted { get; set; }


        }
        public class PermissionModel
        {
            public string PermissionPageName { get; set; }
            public string PageURL { get; set; }
            public bool IsAllowed { get; set; }
        }
        public class UserDetailsModel : AspNetUser
        {
            public string BranchName { get; set; }
            public string BranchCode { get; set; }
            public string EmployeeType { get; set; }

        }
        #endregion

        #region Re-Ordering
        public class ReOrderingViewModel
        {
            public int? BranchID { get; set; }
            public List<BranchTbl> BranchList = new List<BranchTbl>();
            public List<ProductTbl> ProductList = new List<ProductTbl>();
            public List<ProductCategoryTbl> ProductCategoryList = new List<ProductCategoryTbl>();
        }
        #endregion
        #region Rewards Setting
        public class RewardsViewModel
        {
            public int RewardId { get; set; }
            [Required(ErrorMessage = "Please Select  Branch")]
            public int? BranchId { get; set; }
            [Required(ErrorMessage = "Please Enter Reward Minimum Amount")]
            public decimal? RewardMiniumAmount { get; set; }
            //[Required(ErrorMessage = "Please select Item")]
            public List<ProductTbl> ProductList { get; set; }
            public int? RewardItemId { get; set; }
            public string Option { get; set; }
            public bool ItemChecked { get; set; }
            public bool RewardMinimumAmountChecked { get; set; }
            public int? RewardLimit { get; set; }
            public string ProductName { get; set; }
            public int? RewardDetailId { get; set; }

        }
        #endregion

        #region Loyalty Program
        public class LoyaltyProgramViewModel
        {
            public int LoyaltyCardID { get; set; }
            [Required(ErrorMessage = "Please Enter Card Name")]
            public string LoyaltyCardName { get; set; }
            //[Required(ErrorMessage = "Please Select  Branch")]
            public int? BranchId { get; set; }
            [Required(ErrorMessage = "Please Enter Reward Minimum Amount")]
            public decimal? MinimumAmountToGetReward { get; set; }
            [Required(ErrorMessage = "Please select Item")]
            public List<ProductTbl> ProductList { get; set; }
            public int? ProductToClaim { get; set; }
            public string Option { get; set; }
            public bool IsSameClaimeProductToAllBranches { get; set; }

            public bool IsSameMinimumAmountToGetRewardForAllBranches { get; set; }
            [Required(ErrorMessage = "Please Enter Reward Limit To Claim Offer")]
            public int? RewardLimitToClaimOffer { get; set; }
            public string ProductName { get; set; }
            public int? LoyaltyCardDetailId { get; set; }
            public int? LoyaltyCardIDF { get; set; }
            public bool IsApplyToAllBranches { get; set; }

        }
        public class LoyaltyRewardInfoViewModel
        {
            public int LoyaltyCardID { get; set; }
            public string LoyaltyCardName { get; set; }
            public bool CardIsActive { get; set; }
            public int NoOfRewardPointsGot { get; set; }
            public int ClaimOfferOnRewardLimit { get; set; }
            public int IsClaimed { get; set; }
            public int ClaimedProductID { get; set; }
            public string ClaimedProductName { get; set; }
            public bool IsLoyaltyCardExpired { get; set; }
            public bool IsRewardPopupVisible { get; set; }
            public string MessageAboutRewardsForCheckoutPage { get; set; }
        }

        public class LoyaltyRewardSummaryForCheckOutViewModel
        {
            public string CustomerName { get; set; }
            public string ContactNumber { get; set; }
            public int? RewardsEarned { get; set; }
            public int? ClaimProductOn { get; set; }//reward count to get claim
            public string RedeemableProductname { get; set; }
            public bool IsRewardedOrder { get; set; }// for checking the order containes any rewards
            public bool IsRedeemedWithThisOrder { get; set; }// for checking the climable product is claimed with this order
            public LoyaltyCardTbl loyaltyCardInfo = new LoyaltyCardTbl();
            public LoyaltyCardDetailTbl loyaltyCardDetail = new LoyaltyCardDetailTbl();
            public LoyaltyClaimDetailsTbl loyaltyClaimDetails = new LoyaltyClaimDetailsTbl();
            public LoyaltyRewardedOrder loyaltyRewardOrderInfo = new LoyaltyRewardedOrder();

        }

        public class CustomerClaimedRewardsReportViewModel
        {
            public string OrderIDF { get; set; }
            public string ClaimedOn { get; set; }
            public string ProductName { get; set; }
        }

        public class RewardedProductInfo
        {
            public string ProductName { get; set; }
        }
        #endregion

        #region UserRewardsViewModel
        public class UserRewardsViewModel
        {
            public int? RewardedCustomerId { get; set; }
            public string FullBranch { get; set; }
            public string RewardedDate { get; set; }
            public string CustomerName { get; set; }
            public string OrderInvoiceNumber { get; set; }
            public string RewardedMinimumAmount { get; set; }
            public string PaymentAmount { get; set; }
            public int? TotalCount { get; set; }
            public string productName { get; set; }
        }
        #endregion

        #region ComboMeals Settings

        public class ProdcutAdditionItemModel : ProdcutAdditionItemView
        {
            public bool? AdditionItemChecked { get; set; }
        }
        public class ComboMealsProductModel
        {
            public int ProductID { get; set; }
            public decimal? Qty { get; set; }
            public string ProductName { get; set; }
            public string ProductDescription { get; set; }
            public List<ProdcutSauceView> ProductSauceInfo { get; set; }
            public List<ProdcutFillingView> ProductFillingInfo { get; set; }
            public List<ProdcutAdditionItemModel> ProductAdditionalItemInfo { get; set; }
            public int? SauceChecked { get; set; }
            public int? FillingChecked { get; set; }
            public bool? IsOptional { get; set; }
        }
        public class ComboMealsViewModel
        {
            public int ProductID { get; set; }
            public decimal? Price { get; set; }
            public decimal? Qty { get; set; }
            public int MinimumProductCount { get; set; }
            public int? ProductVariantId { get; set; }
            public string ProductName { get; set; }
            public string ProductDescription { get; set; }
            public List<ComboMealsProductModel> ProductInfo { get; set; }
            public int SelectedProductId { get; set; }
        }
        #endregion
    }
}