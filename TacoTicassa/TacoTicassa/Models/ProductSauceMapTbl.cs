//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductSauceMapTbl
    {
        public int ProductSauceMapID { get; set; }
        public Nullable<int> ProductIDf { get; set; }
        public Nullable<int> ProductSauceIDf { get; set; }
        public Nullable<decimal> FreeQty { get; set; }
        public Nullable<bool> IsThisFreeItem { get; set; }
        public Nullable<int> SubProductIDf { get; set; }
    }
}
