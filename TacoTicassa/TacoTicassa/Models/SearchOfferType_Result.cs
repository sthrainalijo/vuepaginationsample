//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TacoTicassa.Models
{
    using System;
    
    public partial class SearchOfferType_Result
    {
        public Nullable<int> TotalCount { get; set; }
        public int ProductOfferTypeID { get; set; }
        public string OfferTypeName { get; set; }
        public string OfferTypeNameArabic { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
