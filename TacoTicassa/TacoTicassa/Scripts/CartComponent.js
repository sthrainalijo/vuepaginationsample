﻿var CartTemplate = '\
    <a href="event.stopPropagation();" class="module module-cart right" data-toggle="panel-cart" v-if="data">\
         <span class="cart-icon">\
         <i class="ti ti-shopping-cart"></i>\
            <span class="notification">{{CartCount}}</span>\
                  </span>\
         <span class="cart-value">AED {{CartAmount}</span>\
         </a>\
';

Vue.component('cart-component', {
    data: function () {
        return {
            CartCount: 0,
            CartAmount: 0
        }
    },
    template: CartTemplate
});
