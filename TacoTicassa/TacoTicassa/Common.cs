﻿using Dapper;
using TacoTicassa.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using static TacoTicassa.Models.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using WebErrorLogging.Utilities;

namespace TacoTicassa
{

   
    public static class UrlHelperExtensions
    {
        public static string ContentVersioned(this UrlHelper self, string contentPath)
        {
            string versionedContentPath = contentPath + "?v=2" + Assembly.GetAssembly(typeof(UrlHelperExtensions)).GetName().Version.ToString();
            return self.Content(versionedContentPath);
        }
    }

    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    public static class Common
    {
        public static string LoginCookieName = "LoginCookiesTacoV1";
        public static Random random = new Random();
        public static bool ConvertCheckboxToBool(string value)
        {
            if (value == "on")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static string TextToPrint(string value)
        {
            if (value == null)
            {
                return "";
            }
            else
            {
                return value.Replace("\n", "</br>");
            }
        }
        public static string ToIndicDigits(this string input)
        {
            return input.Replace('0', '\u0660')
                    .Replace('1', '\u0661')
                    .Replace('2', '\u0662')
                    .Replace('3', '\u0663')
                    .Replace('4', '\u0664')
                    .Replace('5', '\u0665')
                    .Replace('6', '\u0666')
                    .Replace('7', '\u0667')
                    .Replace('8', '\u0668')
                    .Replace('9', '\u0669');
        }
        public static string CovertBoolToString(bool value)
        {
            if(value==true)
            {
                return "on";
            }
            else
            {
                return "off";
            }
        }


        public static void PushNotification( string Heading)
        {
          


            var requests = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
                  
            requests.KeepAlive = true;
            requests.Method = "POST";
            requests.ContentType = "application/json; charset=utf-8";

            requests.Headers.Add("authorization", "Basic NzJmMGRmYTMtMmFiNy00NGJlLWI0MTAtY2RhOTczMDgxNmE5");

            byte[] byteArray = Encoding.UTF8.GetBytes("{"
                                                    + "\"app_id\": \"e2926817-5ec6-4a35-9bec-d33c999dd473\","
                                                  + "\"headings\": {\"en\": \"New Order  \"}," //notification title 
                                                    + "\"url\" : \"https://ordertacodecasa.com/Order/OrderList\"," //redirection link
                                                                                                                   // + "\"chrome_web_image\" : \"http://localhost:61166/Uploads/Thumbnail/" + ImageLink + "\","  //content Image
                                                                                                                   //    + "\"chrome_web_image\" : \"https://media.wired.com/photos/5c1ae77ae91b067f6d57dec0/master/pass/Comparison-City-MAIN-ART.jpg\","  //content Image
                                                    + "\"chrome_web_badge\" : \"https://ordertacodecasa.com/Images/logo.png?v=21.0.0.0\","  //Badge Image
                                                    + "\"contents\": {\"en\": \"" + Heading + "\"}," // content  details
                                                   + "\"include_external_user_ids\": [\"Admin\"]}");

           //+ "\"included_segments\": [\"Active Users\"]}"); // user types
            // + "\"external_user_id\": [\"f45114a5-4102-4ba8-83fa-7b0288d85f35\"]}"); // user types
            string responseContent = null;

            try
            {
                using (var writer = requests.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = requests.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                HelperStoreSqlLog.WriteError(ex, "Error-TacoDeCasa");
                //System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }

            System.Diagnostics.Debug.WriteLine(responseContent);

        }

        public static string ArrayToStringToUseInsideInClause(int?[] List)
        {
            if (List == null)
            {
                return "";
            }

            var inClause = String.Join(",", List.Select(x => x.ToString()).ToArray());

            return inClause;

            //StringBuilder sb = new StringBuilder();
            //foreach (var li in List)
            //{
            //    // IN clause
            //    sb.Append("'"+li.ToString() + "',");
            //}

            //string Output = sb.ToString();
            //Output = Output.TrimEnd(',');

            //return Output;
        }

        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }

        public static object CommonClass { get; internal set; }

        public static string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }
        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string GetConnectionString()
        {
            // Put the name the Sqlconnection from WebConfig..

            var Connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            return Connection;


            //return @"data source=mi3-wsq2.a2hosting.com;initial catalog=techxora_PointOfSale;persist security info=True;user id=PointOfSale;password=TechxoraPOS1;MultipleActiveResultSets=True;";

            // return @"data source=LAPTOP-8MIN2DB3\SA;initial catalog=techxora_PointOfSale;persist security info=True;user id=sa;password=sa.;MultipleActiveResultSets=True;";
            // return @"data source=T1-08\SA2;initial catalog=techxora_PointOfSale;persist security info=True;user id=sa;password=sa.;MultipleActiveResultSets=True;";

        }
        public static bool DeleteImageFolder(string FolderName,string Filename)
        {
            var path = Path.Combine(HttpContext.Current.Server.MapPath(FolderName), Filename);
            if (path != null || path != string.Empty)
            {
                if ((System.IO.File.Exists(path)))
                {
                    System.IO.File.Delete(path);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public static string SaveImageFolder(HttpPostedFileBase Image,string FolderName,int? ID,string SpecificName)
        {
            var Img = Image;
            if (Img != null && Img.ContentLength > 0)
            {
                FileInfo fi = new FileInfo(Img.FileName);
                string ext = fi.Extension;
                var fileName = Path.GetFileName(Img.FileName);
                var ProductCategoryName = Common.GetURLFriendlyString(SpecificName);
                fileName = "Image-" + ID + '-' + ProductCategoryName + ext;
                var path = Path.Combine(HttpContext.Current.Server.MapPath(FolderName), fileName);
                Img.SaveAs(path);

                return fileName;
            }
            else
            {
                return "";
            }
        }

        public static string SaveCompressImageFolder(WebImage Image, string FolderName, int ID, HttpPostedFileBase ImageFileName)
        {
            var fileImg = ImageFileName;
            var Img = Image;
            string random = Common.generateID();
            if (Img != null)
            {
                FileInfo fi = new FileInfo(fileImg.FileName);
                string ext = fi.Extension;
                var fileName = Path.GetFileName(fileImg.FileName);
                //var ProductCategoryName = Common.GetURLFriendlyString(SpecificName);
                fileName = "Image-" + ID + random+ ext;
                var path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(FolderName), fileName);
                Img.Save(path);

                return fileName;
            }
            else
            {
                return "";
            }
        }

        public static string GenerateNotification(string Message, string Type)
        {
            return @"$.notify({message: '" + Message + "'}, {allow_dismiss: true,type: '" + Type + "',placement: {from: 'bottom',align: 'right'}});";
            
        }


        public static string ConvertNumbertoWords(decimal? val)
        {
            decimal FinalValue = 0;
            decimal.TryParse(val.ToString(), out FinalValue);

            long number = (long)FinalValue;

            if (number == 0) return "ZERO";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " LAKH ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            //if ((number / 10) > 0)  
            //{  
            // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
            // number %= 10;  
            //}  
            if (number > 0)
            {
                if (words != "") words += "AND ";
                var unitsMap = new[]
                {
                    "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"
                };
                var tensMap = new[]
                {
                    "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"
                };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }



        public static decimal ConvertToDecial(decimal? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDecimal(value);
            }
        }

        public static decimal ConvertToInt(int? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(value);
            }
        }

        public static string DecimalToPrint(decimal? value)
        {
            if (value == null)
            {
                return "";
            }
            else if (value == 0)
            {
                return "";
            }
            else
            {
                return value.ToString().Replace(".00", "");
            }
        }


        public static string IntToPrint(int? value)
        {
            if (value == null)
            {
                return "";
            }
            else if (value == 0)
            {
                return "";
            }
            else
            {
                return value.ToString();
            }
        }

        public static DateTime GetCurrentDateTime()
        {
            DateTime todaysDate = DateTime.Now;
            var gmTime = todaysDate.ToUniversalTime();
            var indianTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
            var gmTimeConverted = TimeZoneInfo.ConvertTime(gmTime, indianTimeZone);
            DateTime ResultDateTime = Convert.ToDateTime(gmTimeConverted);
            return ResultDateTime;
        }

        public static void SendMail(MailMessage Mail)
        {
            /// Command line argument must the the SMTP host.
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("testman4techxora@gmail.com", "techxora123456");

            Mail.From = new MailAddress("testman4techxora@gmail.com");
            Mail.BodyEncoding = UTF8Encoding.UTF8;
            Mail.IsBodyHtml = true;
            Mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(Mail);
        }

        public static string GetURLFriendlyString(string Value)
        {
            return Regex.Replace(Value, @"[^A-Za-z0-9_\~]+", "-");
        }
        public static string FormatJobCode(int Number)
        {
            var Code = string.Format("{0:0000}", Number);
            return Code;
        }

        public static string GetFormatCode(int ID)
        {
            var Code = string.Format("{0:000}", ID);
            return Code;
        }
        public static string GetSerialFormatCode(int ID)
        {
            var Code = string.Format("{0:00000}", ID);
            return Code;
        }
        public static string GetMonthFormatCode(int ID)
        {
            var Code = string.Format("{0:00}", ID);
            return Code;
        }
        public static int Get2LetterYear(int fourLetterYear)
        {
            return Convert.ToInt32(fourLetterYear.ToString().Substring(2, 2));
        }
        public static void MakeFolderWritable(string Folder)
        {
            if (IsFolderReadOnly(Folder))
            {
                System.IO.DirectoryInfo oDir = new System.IO.DirectoryInfo(Folder);
                oDir.Attributes = oDir.Attributes & ~System.IO.FileAttributes.ReadOnly;
            }
        }
        public static bool IsFolderReadOnly(string Folder)
        {
            System.IO.DirectoryInfo oDir = new System.IO.DirectoryInfo(Folder);
            return ((oDir.Attributes & System.IO.FileAttributes.ReadOnly) > 0);
        }

        //Download to excel
  
        public static LoginInfoModel GetLogInDetails(string UserID)
        {
            string ConnectionString = Common.GetConnectionString();
            using (IDbConnection conn = new SqlConnection(ConnectionString))
            {
                var reader = conn.QueryMultiple("LogInInfo", param: new { UserID = UserID }, commandType: CommandType.StoredProcedure);

                var LoginInfo = reader.Read<LoginInfoModel>().FirstOrDefault();

                return LoginInfo;
            }

        }

        public static LocationViewModel GetCurrentLocation(int? BranchIDf)
        {
            LocationViewModel locationinfo = new LocationViewModel();
            if (BranchIDf!=null)
            {                
                string ConnectionString = Common.GetConnectionString();
                using (IDbConnection conn = new SqlConnection(ConnectionString))
                {
                    string sql = "SELECT BranchAreaID as LocationID, FullArea as LocationName FROM StateBranchView WHERE BranchAreaID = @BranchID";
                    locationinfo = conn.Query<LocationViewModel>(sql, param: new { BranchID = BranchIDf }).FirstOrDefault();
                }
            }
            return (locationinfo);
        }

        //public static string SendEmailForBooking(AppointmentViewModel EmailData)
        //{
        //    var PatientInfo = db.AspNetUsers.Where(x => x.Id == EmailData.PatientIDf).Select(x => new { x.FullName, x.Email }).FirstOrDefault();

        //    var AudiologistName = db.AspNetUsers.Where(x => x.Id == EmailData.AudiologistID && x.UserType == "Audiologist").Select(x => x.FullName).FirstOrDefault();

        //    var HospitalInfo = db.HospitalTbls.Where(x => x.HospitalID == EmailData.HospitalID).FirstOrDefault();
        //}

        public static string SendCredentialsFormat(string Name, string UserName, string Password)
        {
            

            string Content = @"<!DOCTYPE HTML PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                                <html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>

                                <head>
                                    <!--[if gte mso 9]><xml>
                                        <o:OfficeDocumentSettings>
                                        <o:AllowPNG/>
                                        <o:PixelsPerInch>96</o:PixelsPerInch>
                                        </o:OfficeDocumentSettings>
                                    </xml><![endif]-->
                                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                                    <meta name='viewport' content='width=device-width'>
                                    <!--[if !mso]><!-->
                                    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                                    <!--<![endif]-->
                                    <title></title>


                                    <style type='text/css' id='media-query'>
                                    body {
                                        margin: 0;
                                        padding: 0;
                                    }

                                    table,
                                    tr,
                                    td {
                                        vertical-align: top;
                                        border-collapse: collapse;
                                    }

                                    .ie-browser table,
                                    .mso-container table {
                                        table-layout: fixed;
                                    }

                                    * {
                                        line-height: inherit;
                                    }

                                    a[x-apple-data-detectors=true] {
                                        color: inherit !important;
                                        text-decoration: none !important;
                                    }

                                    [owa] .img-container div,
                                    [owa] .img-container button {
                                        display: block !important;
                                    }

                                    [owa] .fullwidth button {
                                        width: 100% !important;
                                    }

                                    [owa] .block-grid .col {
                                        display: table-cell;
                                        float: none !important;
                                        vertical-align: top;
                                    }

                                    .ie-browser .num12,
                                    .ie-browser .block-grid,
                                    [owa] .num12,
                                    [owa] .block-grid {
                                        width: 575px !important;
                                    }

                                    .ExternalClass,
                                    .ExternalClass p,
                                    .ExternalClass span,
                                    .ExternalClass font,
                                    .ExternalClass td,
                                    .ExternalClass div {
                                        line-height: 100%;
                                    }

                                    .ie-browser .mixed-two-up .num4,
                                    [owa] .mixed-two-up .num4 {
                                        width: 188px !important;
                                    }

                                    .ie-browser .mixed-two-up .num8,
                                    [owa] .mixed-two-up .num8 {
                                        width: 376px !important;
                                    }

                                    .ie-browser .block-grid.two-up .col,
                                    [owa] .block-grid.two-up .col {
                                        width: 287px !important;
                                    }

                                    .ie-browser .block-grid.three-up .col,
                                    [owa] .block-grid.three-up .col {
                                        width: 191px !important;
                                    }

                                    .ie-browser .block-grid.four-up .col,
                                    [owa] .block-grid.four-up .col {
                                        width: 143px !important;
                                    }

                                    .ie-browser .block-grid.five-up .col,
                                    [owa] .block-grid.five-up .col {
                                        width: 115px !important;
                                    }

                                    .ie-browser .block-grid.six-up .col,
                                    [owa] .block-grid.six-up .col {
                                        width: 95px !important;
                                    }

                                    .ie-browser .block-grid.seven-up .col,
                                    [owa] .block-grid.seven-up .col {
                                        width: 82px !important;
                                    }

                                    .ie-browser .block-grid.eight-up .col,
                                    [owa] .block-grid.eight-up .col {
                                        width: 71px !important;
                                    }

                                    .ie-browser .block-grid.nine-up .col,
                                    [owa] .block-grid.nine-up .col {
                                        width: 63px !important;
                                    }

                                    .ie-browser .block-grid.ten-up .col,
                                    [owa] .block-grid.ten-up .col {
                                        width: 57px !important;
                                    }

                                    .ie-browser .block-grid.eleven-up .col,
                                    [owa] .block-grid.eleven-up .col {
                                        width: 52px !important;
                                    }

                                    .ie-browser .block-grid.twelve-up .col,
                                    [owa] .block-grid.twelve-up .col {
                                        width: 47px !important;
                                    }

                                    @media only screen and (min-width: 595px) {
                                        .block-grid {
                                        width: 575px !important;
                                        }
                                        .block-grid .col {
                                        vertical-align: top;
                                        }
                                        .block-grid .col.num12 {
                                        width: 575px !important;
                                        }
                                        .block-grid.mixed-two-up .col.num4 {
                                        width: 188px !important;
                                        }
                                        .block-grid.mixed-two-up .col.num8 {
                                        width: 376px !important;
                                        }
                                        .block-grid.two-up .col {
                                        width: 287px !important;
                                        }
                                        .block-grid.three-up .col {
                                        width: 191px !important;
                                        }
                                        .block-grid.four-up .col {
                                        width: 143px !important;
                                        }
                                        .block-grid.five-up .col {
                                        width: 115px !important;
                                        }
                                        .block-grid.six-up .col {
                                        width: 95px !important;
                                        }
                                        .block-grid.seven-up .col {
                                        width: 82px !important;
                                        }
                                        .block-grid.eight-up .col {
                                        width: 71px !important;
                                        }
                                        .block-grid.nine-up .col {
                                        width: 63px !important;
                                        }
                                        .block-grid.ten-up .col {
                                        width: 57px !important;
                                        }
                                        .block-grid.eleven-up .col {
                                        width: 52px !important;
                                        }
                                        .block-grid.twelve-up .col {
                                        width: 47px !important;
                                        }
                                    }

                                    @media (max-width: 595px) {
                                        .block-grid,
                                        .col {
                                        min-width: 320px !important;
                                        max-width: 100% !important;
                                        display: block !important;
                                        }
                                        .block-grid {
                                        width: calc(100% - 40px) !important;
                                        }
                                        .col {
                                        width: 100% !important;
                                        }
                                        .col>div {
                                        margin: 0 auto;
                                        }
                                        img.fullwidth,
                                        img.fullwidthOnMobile {
                                        max-width: 100% !important;
                                        }
                                        .no-stack .col {
                                        min-width: 0 !important;
                                        display: table-cell !important;
                                        }
                                        .no-stack.two-up .col {
                                        width: 50% !important;
                                        }
                                        .no-stack.mixed-two-up .col.num4 {
                                        width: 33% !important;
                                        }
                                        .no-stack.mixed-two-up .col.num8 {
                                        width: 66% !important;
                                        }
                                        .no-stack.three-up .col.num4 {
                                        width: 33% !important;
                                        }
                                        .no-stack.four-up .col.num3 {
                                        width: 25% !important;
                                        }
                                        .mobile_hide {
                                        min-height: 0px;
                                        max-height: 0px;
                                        max-width: 0px;
                                        display: none;
                                        overflow: hidden;
                                        font-size: 0px;
                                        }
                                    }
                                    </style>
                                </head>

                                <body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF'>
                                    <style type='text/css' id='media-query-bodytag'>
                                    @media (max-width: 520px) {
                                        .block-grid {
                                        min-width: 320px !important;
                                        max-width: 100% !important;
                                        width: 100% !important;
                                        display: block !important;
                                        }

                                        .col {
                                        min-width: 320px !important;
                                        max-width: 100% !important;
                                        width: 100% !important;
                                        display: block !important;
                                        }

                                        .col>div {
                                        margin: 0 auto;
                                        }

                                        img.fullwidth {
                                        max-width: 100% !important;
                                        }
                                        img.fullwidthOnMobile {
                                        max-width: 100% !important;
                                        }
                                        .no-stack .col {
                                        min-width: 0 !important;
                                        display: table-cell !important;
                                        }
                                        .no-stack.two-up .col {
                                        width: 50% !important;
                                        }
                                        .no-stack.mixed-two-up .col.num4 {
                                        width: 33% !important;
                                        }
                                        .no-stack.mixed-two-up .col.num8 {
                                        width: 66% !important;
                                        }
                                        .no-stack.three-up .col.num4 {
                                        width: 33% !important;
                                        }
                                        .no-stack.four-up .col.num3 {
                                        width: 25% !important;
                                        }
                                        .mobile_hide {
                                        min-height: 0px !important;
                                        max-height: 0px !important;
                                        max-width: 0px !important;
                                        display: none !important;
                                        overflow: hidden !important;
                                        font-size: 0px !important;
                                        }
                                    }
                                    </style>
                                    <!--[if IE]><div class='ie-browser'><![endif]-->
                                    <!--[if mso]><div class='mso-container'><![endif]-->
                                    <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%'
                                    cellpadding='0' cellspacing='0'>
                                    <tbody>
                                        <tr style='vertical-align: top'>
                                        <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                                            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td align='center' style='background-color: #FFFFFF;'><![endif]-->

                                            <div style='background-color:transparent;'>
                                            <div style='Margin: 0 auto;min-width: 320px;max-width: 575px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;'
                                                class='block-grid '>
                                                <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
                                                <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 575px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                                                <!--[if (mso)|(IE)]><td align='center' width='575' style=' width:575px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
                                                <div class='col num12' style='min-width: 320px;max-width: 575px;display: table-cell;vertical-align: top;'>
                                                    <div style='background-color: transparent; width: 100% !important;'>
                                                    <!--[if (!mso)&(!IE)]><!-->
                                                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;'>
                                                        <!--<![endif]-->


                                                        <div class=''>
                                                        <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
                                                        <div style='color:#00d2b9;font-family:Georgia, Times, 'Times New Roman', serif;line-height:120%; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
                                                            <div style='font-size:12px;line-height:14px;font-family:Georgia, Times, 'Times New Roman', serif;color:#00d2b9;text-align:left;'>
                                                            <p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'>
                                                                <span style='font-size: 38px; line-height: 45px;'>
                                                                <span style='line-height: 45px; font-size: 38px;'>Power House Gym</span>
                                                                </span>
                                                            </p>
                                                            <p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'>
                                                                <em>
                                                                <span style='color: rgb(85, 85, 85); font-size: 80px; line-height: 96px;'>
                                                                    <span style='line-height: 96px; font-size: 80px;'>
                                                                    <span style='line-height: 96px; font-size: 80px;'>" + Name + "</span>";

            Content += @"</span>
                                                                </span>
                                                                </em>
                                                            </p>
                                                            </div>
                                                        </div>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                        </div>


                                                        <div class=''>
                                                        <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 30px; padding-left: 30px; padding-top: 20px; padding-bottom: 15px;'><![endif]-->
                                                        <div style='color:#555555;font-family:Georgia, Times, 'Times New Roman', serif;line-height:120%; padding-right: 30px; padding-left: 30px; padding-top: 20px; padding-bottom: 15px;'>
                                                            <div style='font-family:Georgia, Times, 'Times New Roman', serif;font-size:12px;line-height:14px;color:#555555;text-align:left;'>
                                                            <p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>
                                                                <em>
                                                                <span style='font-size: 34px; line-height: 40px;'>Log in with the details below</span>
                                                                </em>
                                                            </p>
                                                            </div>
                                                        </div>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                        </div>



                                                        <div align='center' class='button-container center ' style='padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;'>
                                                        <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;' align='center'><v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='https://powerhouse.techxora.com' style='height:31pt; v-text-anchor:middle; width:133pt;' arcsize='10%' strokecolor='#3AAEE0' fillcolor='#3AAEE0'><w:anchorlock/><v:textbox inset='0,0,0,0'><center style='color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:16px;'><![endif]-->
                                                        <a href='https://powerhouse.techxora.com' target='_blank' style='display: block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #ffffff; background-color: #3AAEE0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 178px; width: 138px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;mso-border-alt: none'>
                                                            <span style='font-size:16px;line-height:32px;'>Click here to Login</span>
                                                        </a>
                                                        <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                                        </div>



                                                        <div class=''>
                                                        <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
                                                        <div style='color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
                                                            <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;'>
                                                            <p style='margin: 0;font-size: 14px;line-height: 17px'>
                                                                <strong>User Name</strong> : " + UserName + "</p>";

            Content += @"<p style='margin: 0;font-size: 14px;line-height: 17px'>
                                                                <br data-mce-bogus='1'>
                                                            </p>
                                                            <p style='margin: 0;font-size: 14px;line-height: 17px'>
                                                                <strong>Password</strong> : " + Password + "</p>";
            Content += @"</div>
                                                        </div>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                        </div>

                                                        <!--[if (!mso)&(!IE)]><!-->
                                                    </div>
                                                    <!--<![endif]-->
                                                    </div>
                                                </div>
                                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                                </div>
                                            </div>
                                            </div>
                                            <div style='background-color:#00d2b9;'>
                                            <div style='Margin: 0 auto;min-width: 320px;max-width: 575px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;'
                                                class='block-grid '>
                                                <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
                                                <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:#00d2b9;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 575px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                                                <!--[if (mso)|(IE)]><td align='center' width='575' style=' width:575px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
                                                <div class='col num12' style='min-width: 320px;max-width: 575px;display: table-cell;vertical-align: top;'>
                                                    <div style='background-color: transparent; width: 100% !important;'>
                                                    <!--[if (!mso)&(!IE)]><!-->
                                                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
                                                        <!--<![endif]-->



                                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' class='divider ' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                                        <tbody>
                                                            <tr style='vertical-align: top'>
                                                            <td class='divider_inner' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                                                <table class='divider_content' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                                                <tbody>
                                                                    <tr style='vertical-align: top'>
                                                                    <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                                                        <span></span>
                                                                    </td>
                                                                    </tr>
                                                                </tbody>
                                                                </table>
                                                            </td>
                                                            </tr>
                                                        </tbody>
                                                        </table>

                                                        <!--[if (!mso)&(!IE)]><!-->
                                                    </div>
                                                    <!--<![endif]-->
                                                    </div>
                                                </div>
                                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                                </div>
                                            </div>
                                            </div>
                                            <div style='background-color:#F2F8F9;'>
                                            <div style='Margin: 0 auto;min-width: 320px;max-width: 575px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;'
                                                class='block-grid '>
                                                <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
                                                <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:#F2F8F9;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 575px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                                                <!--[if (mso)|(IE)]><td align='center' width='575' style=' width:575px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid #5ACEE1; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
                                                <div class='col num12' style='min-width: 320px;max-width: 575px;display: table-cell;vertical-align: top;'>
                                                    <div style='background-color: transparent; width: 100% !important;'>
                                                    <!--[if (!mso)&(!IE)]><!-->
                                                    <div style='border-top: 0px solid #5ACEE1; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
                                                        <!--<![endif]-->


                                                        &#160;

                                                        <!--[if (!mso)&(!IE)]><!-->
                                                    </div>
                                                    <!--<![endif]-->
                                                    </div>
                                                </div>
                                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                                </div>
                                            </div>
                                            </div>
                                            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <!--[if (mso)|(IE)]></div><![endif]-->


                                </body>

                                </html>";

            return Content;
        }
    }
}